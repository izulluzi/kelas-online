// LOCAL
var base_url = location.origin + "/kelas_online";

/*//ONLINE
var base_url = location.origin;*/

$('input').attr('autocomplete','off');
$('#FrmUserLogin input').attr('autocomplete','on');
if($("html").find(".select").hasClass("select")) {
    $(".select").select2({
        minimumResultsForSearch: -1
    });
}

var datanext = 0, dataprev = 0;
var request  = {"filter": {"kywd": ""}};
var act = "", getfunc = "", interval_total_chat;
var l = $(".ladda-button-submit").ladda();

$(".modal").on('show.bs.modal', function () {
    $(this).find(".modal-body").css({"position": "relative", "overflow-y": "scroll", "max-height": ($(window).height()-160)});
    if($(this).find(".modal-footer").hasClass("modal-footer")) {
        $(this).find(".modal-body").css({"position": "relative", "overflow-y": "scroll", "max-height": ($(window).height()-260)});
    }
    if($(this).attr("id").indexOf("modal-status-") != "-1"){
        $(this).find(".modal-body").css("overflow", "inherit");
    }
    if($(this).find("form").attr("id") != undefined){
        $(this).find(".modal-body").css("overflow-y", "scroll");
        if($(this).find("form").attr("id").indexOf("Status") != "-1"){
            $(this).find(".modal-body").css("overflow", "inherit");
        }
    }
    setTimeout(function(){
        $("body").removeClass("modal-open");
    }, 600);
});

function uploadfile(selectorform, successfunc, action) {
    successfunc = (typeof successfunc !== 'undefined') ?  successfunc : "";
    var formdata = $(selectorform).serialize() +"&act=uploaddata";
    var formaction = $(selectorform).attr("action");
    $.ajax({
        type : "post",
        url: base_url + formaction,
        data :formdata,
        cache : false,
        dataType: 'json',
        beforeSend: function() {
            l.ladda("start");
        },
        success : function(resp){
            l.ladda("stop");
            if(resp.IsError == false) {
                toastrshow("success", "File sukses upload", "Success");
                $(".responfilehidden").val(resp.Output);
                $("#FrmProsesExcel").submit();
            } else {
                toastrshow("warning", resp.Output, "Warning");
            }
        }
    });
}

function toastrshow(type, title, message) {
    message = (typeof message !== 'undefined') ?  message : "";
    toastr.options = {
        closeButton: true,
        progressBar: true,
        showMethod: "slideDown",
        positionClass: "toast-top-right",
        timeOut: 4000,
        onclick: null,
        showMethod: "fadeIn",
        hideMethod: "fadeOut",
    }
    switch(type) {
        case "success" : toastr["success"](title, message);  break;
        case "info"    : toastr["info"](title, message);     break;
        case "warning" : toastr["warning"](title, message);  break;
        case "error"   : toastr["error"](title, message);    break;
        default        : toastr["info"](title, message);     break;
    }
}

$('.table-responsive').on('show.bs.dropdown', function () {
    $('.table-responsive').css( "overflow", "inherit" );
});

$('.table-responsive').on('hide.bs.dropdown', function () {
    $('.table-responsive').css( "overflow", "auto" );
});


function GetDropdownUser(selected, kategori, successfunc, target, request_data_asesi) {
    selected = (typeof selected !== 'undefined') ?  selected : "";
    successfunc = (typeof successfunc !== 'undefined') ?  successfunc : "";
    request_data_asesi = (typeof request_data_asesi !== 'undefined') ?  request_data_asesi : "";
    request["Sort"] = "";
    request = request_data_asesi;
    req = request;
    $.ajax({
        type: "POST",
        url: base_url + "/data_user/ajax_data_user",
        data: {act:"datadropdown", req:req},
        dataType: "JSON",
        tryCount: 0,
        retryLimit : 3,
        success: function(resp){
            if(target == "" || target == null || target == undefined){
                if(resp.lsdt && resp.lsdt != "undefined") {
                    if(typeof $(".select2.dropdown-data-user").attr("multiple") !== typeof undefined && $(".select2.dropdown-data-user").attr("multiple") !== false) {
                        var result  = resp.lsdt;
                    } else {
                        var result  = "<option value=''>Pilih User</option>";
                        result += resp.lsdt;
                    }
                    $(".dropdown-data-user").html(result);
                    if(selected != "") {
                        $(".dropdown-data-user").val(selected).trigger("change");
                    }
                    if(successfunc != "") {
                        successfunc(resp);
                    }
                }
                $(".select2.dropdown-data-user").select2({
                    disabled: false
                });
                $(".loading-dropdown-data-user").addClass("hidden");
            } else {
                if(resp.lsdt && resp.lsdt != "undefined") {
                    var result  = "<option value=''>Pilih User</option>";
                        result += resp.lsdt;
                    $(".dropdown-data-user-"+target).html(result);
                    if(selected != "") {
                        $(".dropdown-data-user-"+target).val(selected).trigger("change");
                    }
                    if(successfunc != "") {
                        successfunc(resp);
                    }
                }
                $(".select2.dropdown-data-user-"+target).select2({
                    disabled: false
                });
                $(".loading-dropdown-data-user-"+target).addClass("hidden");
            }
        },
        error: function(xhr, textstatus, errorthrown) {
            if(target == "" || target == null || target == undefined){
                $(".select2.dropdown-data-user").select2({
                    disabled: true,
                    placeholder: "Periksa koneksi internet anda kembali",
                });
                $(".loading-dropdown-data-user").addClass("hidden");
            } else {
                $(".select2.dropdown-data-user-"+target).select2({
                    disabled: true,
                    placeholder: "Periksa koneksi internet anda kembali",
                });
                $(".loading-dropdown-data-user-"+target).addClass("hidden");
            }
        }
    });
}

function validatedata(evt) {
    var charCode = (evt.which) ? evt.which : evt.keyCode;
    return !(charCode > 31 && (charCode < 48 || charCode > 57));
}

function rupiah(number){
    if(number == null || number == ""){
        return 0;
    }
    number = number.toString().replace(".00", "");
    number = number.replace(".", "");
    var minus_return = "";
    if(number.indexOf("-") != -1){
        number = number.replace("-", "");
        minus_return = "-";
    }
    var inputan = Number(number);
    var number_string = inputan.toString(), sisa = number_string.length % 3, inputan = number_string.substr(0, sisa), ribuan = number_string.substr(sisa).match(/\d{3}/g); if (ribuan) { separator = sisa ? '.' : ''; inputan += separator + ribuan.join('.'); }
    return minus_return+inputan;
}

function backAway() {
    if(history.length === 1){
        history.back();
    } else {
        history.back();
    }
}

function loader(withtable, colspan) {
    colspan = (colspan != "") ? colspan : "10";
    withtable = (typeof withtable !== 'undefined') ?  withtable : false;
    var html  = '';
    if(withtable == true) html += "<tr><td colspan='"+colspan+"' class='text-center'>";
    html += '<center><img class="loading-gif-image" src="'+base_url+'/assets/img/loading-data.gif" alt="Loading ..."></center>';
    if(withtable == true) html += "</td>";
    return html;
}

function resetformvalue(selector) {
    $(selector).trigger("reset"); //Reset value di form. Kecuali Select2
    $(selector + " select").val("").trigger("change"); //Reset seluruh Select2 yang ada di form
}

function GetData(req, action, table, successfunc, colspan) {
    colspan = (colspan != "") ? colspan : "10";
    req = (typeof req !== 'undefined') ?  req : "";
    successfunc = (typeof successfunc !== 'undefined') ?  successfunc : "";
    act = (action != "") ? action : "listdatahtml";
    $(".datatable-"+table+" tbody").html(loader(true, colspan));
    $.ajax({
        type: "POST",
        url: base_url + pagename,
        data: {act:act, req:req},
        dataType: "JSON",
        tryCount: 0,
        retryLimit: 3,
        success: function(resp){
            if(resp.paging.Total != undefined) {
                $(".datatable-"+table+" tbody").html(resp.lsdt);
                pagination(resp.paging, table);
                if(successfunc != "") {
                    getfunc = successfunc;
                    successfunc(resp);
                }
            } else {
                $(".datatable-"+table+" tbody").html(resp.lsdt);
                $(".pagination-layout-"+table).addClass("d-none");
                if(successfunc != "") {
                    getfunc = successfunc;
                    successfunc(resp);
                }
            }
        },
        error: function(xhr, textstatus, errorthrown) {
            $(".datatable-"+table+" tbody").html("<tr><td colspan='"+colspan+"' class='text-center'><span class='badge badge-pill badge-warning'>Periksa koneksi internet anda kembali</span></td></tr>");
            $(".pagination-layout-"+table).addClass("d-none");
        }
    });
}

function GetDataById(id, successfunc, action, errorfunc) {
    action = (typeof action !== 'undefined') ?  action : "getdatabyid";
    $.ajax({
        type: "POST",
        url: base_url + pagename,
        data: {"act":action, req:id},
        tryCount: 0,
        retryLimit: 3,
        success: function(resp){
            resp = JSON.parse(resp);
            if(resp.length == 0 || resp.data == "") {
                $(".modal").modal("hide");
                toastrshow("warning", "Data tidak ditemukan", "Peringatan");
            } else {
                successfunc(resp);
            }
        },
        error: function(xhr, textstatus, errorthrown) {
            setTimeout(function(){
                $(".modal").modal("hide");
                toastrshow("warning", "Periksa koneksi internet anda kembali", "Peringatan");
            }, 500);
        }
    });
}

function InsertData(selectorform, successfunc, errorfunc) {
    successfunc = (typeof successfunc !== 'undefined') ?  successfunc : "";
    errorfunc = (typeof errorfunc !== 'undefined') ?  errorfunc : "";
    var formdata   = $(selectorform).serialize() +"&act=insertdata";
    var formaction = $(selectorform).attr("action");
    $.ajax({
        type: "POST",
        url: base_url + formaction,
        data: formdata,
        dataType: "JSON",
        tryCount: 0,
        retryLimit: 3,
        beforeSend: function() {
            laddasubmit.ladda("start");
        },
        success: function(resp){
            laddasubmit.ladda("stop");
            if(resp.IsError != undefined) {
                if(resp.ErrorMessage[0].error != undefined) {
                    toastrshow("warning", resp.ErrorMessage[0].error, "Peringatan");
                } else {
                    toastrshow("warning", resp.ErrorMessage, "Peringatan");
                }
            } else {
                if(resp == 1 || resp == "1") {
                    toastrshow("success", "Data berhasil disimpan", "Success");
                    $(selectorform).parents(".modal").modal("hide"); //Tutup modal
                    if(successfunc != "") {
                        successfunc(resp);
                    }
                } else {
                    toastrshow("warning", "Data gagal disimpan", "Peringatan");
                }
            }
        },
        error: function(xhr, textstatus, errorthrown) {
            laddasubmit.ladda("stop");
            setTimeout(function(){
                $(".modal").modal("hide");
                toastrshow("warning", "Periksa koneksi internet anda kembali", "Peringatan");
            }, 500);
        }
    });
}

function UpdateData(selectorform, successfunc, errorfunc) {
    successfunc = (typeof successfunc !== 'undefined') ?  successfunc : "";
    errorfunc = (typeof errorfunc !== 'undefined') ?  errorfunc : "";
    var formdata   = $(selectorform).serialize() +"&act=updatedata";
    var formaction = $(selectorform).attr("action");
    $.ajax({
        type: "POST",
        url: base_url + formaction,
        data: formdata,
        dataType: "JSON",
        tryCount: 0,
        retryLimit: 3,
        beforeSend: function() {
            laddasubmit.ladda("start");
        },
        success: function(resp){
            laddasubmit.ladda("stop");
            if(resp.IsError != undefined) {
                if(resp.ErrorMessage[0].error != undefined) {
                    toastrshow("warning", resp.ErrorMessage[0].error, "Peringatan");
                } else {
                    toastrshow("warning", resp.ErrorMessage, "Peringatan");
                }
            } else {
                if(resp == 1 || resp == "1") {
                    toastrshow("success", "Data berhasil disimpan", "Success");
                    $(selectorform).parents(".modal").modal("hide"); //Tutup modal
                    if(successfunc != "") {
                        successfunc(resp);
                    }
                } else {
                    toastrshow("error", "Data gagal disimpan", "Peringatan");
                    if(errorfunc != "") {
                        errorfunc();
                    }
                }
            }
        },
        error: function(xhr, textstatus, errorthrown) {
            laddasubmit.ladda("stop");
            setTimeout(function(){
                $(".modal").modal("hide");
                toastrshow("warning", "Periksa koneksi internet anda kembali", "Peringatan");
            }, 500);
        }
    });
}

function DeleteData(selectorform, successfunc, errorfunc) {
    successfunc = (typeof successfunc !== 'undefined') ?  successfunc : "";
    errorfunc = (typeof errorfunc !== 'undefined') ?  errorfunc : "";
    var formdata   = $(selectorform).serialize() +"&act=deletedata";
    var formaction = $(selectorform).attr("action");
    $.ajax({
        type: "POST",
        url: base_url + formaction,
        data: formdata,
        dataType: "JSON",
        tryCount: 0,
        retryLimit: 3,
        beforeSend: function() {
            laddasubmit.ladda("start");
        },
        success: function(resp){
            laddasubmit.ladda("stop");
            if(resp == 1 || resp == "1") {
                toastrshow("success", "Data berhasil dihapus", "Success");
                $(selectorform).parents(".modal").modal("hide"); //Tutup modal
                if(successfunc != "") {
                    successfunc(resp);
                }
            } else {
                toastrshow("error", "Data gagal dihapus", "Peringatan");
                if(errorfunc != "") {
                    errorfunc();
                }
            }
        },
        error: function(xhr, textstatus, errorthrown) {
            laddasubmit.ladda("stop");
            setTimeout(function(){
                $(".modal").modal("hide");
                toastrshow("warning", "Periksa koneksi internet anda kembali", "Peringatan");
            }, 500);
        }
    });
}

// START PAGINATION -------------------------------------------------------------------
function pagination(page, table) {
    var paginglayout = $(".pagination-layout-"+table);
    var infopage = page.InfoPage+" Records | "+page.JmlHalTotal+" Pages";
    
    paginglayout.removeClass("d-none");
    paginglayout.find("input[type='text']").val(Number(page.HalKe));
    paginglayout.find("div.info").html(infopage);
    if(page.IsNext == true) {
        paginglayout.find(".btn.next, .next-head").removeClass("disabled").removeAttr("disabled");
        paginglayout.find(".btn.last").removeClass("disabled").removeAttr("disabled");
        paginglayout.find(".btn.last").attr("lastpage", page.JmlHalTotal);
        datanext = (Number(page.HalKe) + 1);
    } else {
        paginglayout.find(".btn.next, .next-head").addClass("disabled").attr("disabled", "disabled");
        paginglayout.find(".btn.last").addClass("disabled").attr("disabled", "disabled");
        dataprev = 0;
    }
    if(page.IsPrev == true) {
        paginglayout.find(".btn.prev, .prev-head").removeClass("disabled").removeAttr("disabled");
        paginglayout.find(".btn.first").removeClass("disabled").removeAttr("disabled");
        dataprev = (Number(page.HalKe) - 1);
    } else {
        paginglayout.find(".btn.prev, .prev-head").addClass("disabled").attr("disabled", "disabled");
        paginglayout.find(".btn.first").addClass("disabled").attr("disabled", "disabled");
        dataprev = 0;
    }
}
function setpagination(page, table) {
    var paginglayout = $(".set-pagination-layout-"+table);
    var infopage = page.InfoPage+" Records | "+page.JmlHalTotal+" Pages";
    paginglayout.removeClass("d-none");
    paginglayout.find("input[type='text']").val(Number(page.HalKe));
    paginglayout.find("div.info").html(infopage);
    if(page.IsNext == true) {
        paginglayout.find(".btn.next").removeClass("disabled");
        paginglayout.find(".btn.last").removeClass("disabled");
        paginglayout.find(".btn.last").attr("lastpage", page.JmlHalTotal);
        datanext = (Number(page.HalKe) + 1);
    } else {
        paginglayout.find(".btn.next").addClass("disabled");
        paginglayout.find(".btn.last").addClass("disabled");
        dataprev = 0;
    }
    if(page.IsPrev == true) {
        paginglayout.find(".btn.prev").removeClass("disabled");
        paginglayout.find(".btn.first").removeClass("disabled");
        dataprev = (Number(page.HalKe) - 1);
    } else {
        paginglayout.find(".btn.prev").addClass("disabled");
        paginglayout.find(".btn.first").addClass("disabled");
        dataprev = 0;
    }
}
$(".btn.next").click(function() {
    var table = $(this).parent().parent().parent().parent().attr("class");
    var classdata = table.replace(/modal-footer set-pagination-layout-/g, "", table);
    table = table.replace(classdata, "", table);
    if(table == "modal-footer set-pagination-layout-"){
        pagename = $(this).parent().parent().parent().attr("pagename");
        request["Page"] = datanext;
        GetListGroup(request, act, classdata, getfunc);
    } else {
        pagename = $(this).parent().parent().parent().parent().parent().parent().attr("pagename");
        colspan = $(this).parent().parent().parent().parent().parent().parent().attr("data-colspan");
        var table = $(this).parent().parent().parent().parent().parent().parent().attr("class");
        table = table.replace(/pagination-layout-/g, "", table);
        request["Page"] = datanext;
        GetData(request, act, table, getfunc, colspan);
    }
});
$(".btn.prev").click(function() {
    var table = $(this).parent().parent().parent().parent().attr("class");
    var classdata = table.replace(/modal-footer set-pagination-layout-/g, "", table);
    table = table.replace(classdata, "", table);
    if(table == "modal-footer set-pagination-layout-"){
        pagename = $(this).parent().parent().parent().attr("pagename");
        request["Page"] = dataprev;
        GetListGroup(request, act, classdata, getfunc);
    } else {
        pagename = $(this).parent().parent().parent().parent().parent().parent().attr("pagename");
        colspan = $(this).parent().parent().parent().parent().parent().parent().attr("data-colspan");
        var table = $(this).parent().parent().parent().parent().parent().parent().attr("class");
        table = table.replace(/pagination-layout-/g, "", table);
        request["Page"] = dataprev;
        GetData(request, act, table, getfunc, colspan);
    }
});
$(".btn.first").click(function() {
    var table = $(this).parent().parent().parent().parent().parent().parent().attr("class");
    table = table.replace(/pagination-layout-/g, "", table);
    request["Page"] = 1;
    pagename = $(this).parent().parent().parent().parent().parent().parent().attr("pagename");
    colspan = $(this).parent().parent().parent().parent().parent().parent().attr("data-colspan");
    GetData(request, act, table, getfunc, colspan);
});
$(".btn.last").click(function() {
    var table = $(this).parent().parent().parent().parent().parent().parent().attr("class");
    table = table.replace(/pagination-layout-/g, "", table);
    request["Page"] = $(this).attr('lastpage');
    pagename = $(this).parent().parent().parent().parent().parent().parent().attr("pagename");
    colspan = $(this).parent().parent().parent().parent().parent().parent().attr("data-colspan");
    GetData(request, act, table, getfunc, colspan);
});
$(".limit").change(function() {
    var table = $(this).parent().parent().parent().parent().attr("class");
    table = table.replace(/pagination-layout-/g, "", table);
    var limit = $(this).val();
    pagename = $(this).parent().parent().parent().parent().attr("pagename");
    colspan = $(this).parent().parent().parent().parent().attr("data-colspan");
    request["Limit"] = limit;
    GetData(request, act, table, getfunc, colspan);
});
$("#FrmGotoPage").submit(function() {
    var table = $(this).parent().parent().parent().attr("class");
    table = table.replace(/pagination-layout-/g, "", table);
    var page = $(this).find("input[type='text']").val();
    request["Page"] = page;
    pagename = $(this).parent().parent().parent().attr("pagename");
    colspan = $(this).parent().parent().parent().attr("data-colspan");
    GetData(request, act, table, getfunc, colspan);
    return false;
});

$(".btn.next-head").click(function() {
    var table = $(this).parent().attr("class");
    table = table.replace(/btn-group pagination-layout-/g, "", table);
    request["Page"] = datanext;
    pagename = $(this).parent().attr("pagename");
    colspan = $(this).parent().attr("data-colspan");
    GetData(request, act, table, getfunc, colspan);
});
$(".btn.prev-head").click(function() {
    var table = $(this).parent().attr("class");
    table = table.replace(/btn-group pagination-layout-/g, "", table);
    request["Page"] = dataprev;
    pagename = $(this).parent().attr("pagename");
    colspan = $(this).parent().attr("data-colspan");
    GetData(request, act, table, getfunc, colspan);
});
// END PAGINATION ---------------------------------------------------------------------