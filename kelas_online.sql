/*
 Navicat Premium Data Transfer

 Source Server         : localhost
 Source Server Type    : MySQL
 Source Server Version : 100139
 Source Host           : localhost:3306
 Source Schema         : kelas_online

 Target Server Type    : MySQL
 Target Server Version : 100139
 File Encoding         : 65001

 Date: 07/07/2019 04:02:12
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for tb_admin
-- ----------------------------
DROP TABLE IF EXISTS `tb_admin`;
CREATE TABLE `tb_admin`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nama` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  `username` varchar(30) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  `password` varchar(95) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  `no_telp_hp` varchar(20) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `email` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `jk` tinyint(1) NOT NULL,
  `foto` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  PRIMARY KEY (`id`, `username`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 4 CHARACTER SET = latin1 COLLATE = latin1_swedish_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Records of tb_admin
-- ----------------------------
INSERT INTO `tb_admin` VALUES (1, 'Muhammad Waizulkarnain', 'izul', '$argon2i$v=19$m=2048,t=4,p=3$MC5tRWkuVThkMnE1L09mMQ$33Vr9iAJYySDksYtk77/znC1ieEjRSluMQmv77PRU74', '085728142457', 'izul@gmail.com', 1, '2019.05.05_18.06.58.5ccec3d2b7975.jpg');
INSERT INTO `tb_admin` VALUES (3, 'Admin E-Kelas', 'admin', '$argon2i$v=19$m=2048,t=4,p=3$VFBxUURLQW9ISHBveE5oVQ$iw5mb4llx5DC6SoIB2jRUEOcknNo6FmDUWMDLT24i1U', '', '', 1, '2019.07.07_03.58.25.5d210b71d8cf0.png');

-- ----------------------------
-- Table structure for tb_kelas
-- ----------------------------
DROP TABLE IF EXISTS `tb_kelas`;
CREATE TABLE `tb_kelas`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nama` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  `keterangan` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `id_user` text CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 13 CHARACTER SET = latin1 COLLATE = latin1_swedish_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Records of tb_kelas
-- ----------------------------
INSERT INTO `tb_kelas` VALUES (4, 'XII A', 'Kelas 12 Alfa', '1, 2, 9, 10, 11, 12');
INSERT INTO `tb_kelas` VALUES (5, 'XII B', 'Kelas 12 Bravo', '8, 9, 10, 12, 13, 14');
INSERT INTO `tb_kelas` VALUES (6, 'XII C', 'Kelas 12 Catur', '1, 3, 5, 9, 11, 12, 13');
INSERT INTO `tb_kelas` VALUES (7, 'XII D', 'Kelas 12 Delta', '1, 8, 10, 13');
INSERT INTO `tb_kelas` VALUES (8, 'XII E', 'Kelas 12 Elit', '5, 8, 9, 10');
INSERT INTO `tb_kelas` VALUES (9, 'XII F', 'Kelas 12 Flamingo', '1, 2, 3, 4, 5, 10, 11, 12, 13, 14');
INSERT INTO `tb_kelas` VALUES (10, 'XII G', 'Kelas 12 Gajah', '4, 8, 9, 10, 12');
INSERT INTO `tb_kelas` VALUES (11, 'XII I', 'Kelas 11 Iguana', '1, 10, 11');
INSERT INTO `tb_kelas` VALUES (12, 'XII J', 'Kelas 12 Jambu', '1, 2, 3, 4, 8, 11');

-- ----------------------------
-- Table structure for tb_user
-- ----------------------------
DROP TABLE IF EXISTS `tb_user`;
CREATE TABLE `tb_user`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nama` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  `email` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  `jk` tinyint(1) NOT NULL,
  `no_telp_hp` varchar(20) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `foto` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `alamat` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 15 CHARACTER SET = latin1 COLLATE = latin1_swedish_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Records of tb_user
-- ----------------------------
INSERT INTO `tb_user` VALUES (1, 'Dimas Agung Wisesa', 'dimas@gmail.com', 1, '085728142457', '2019.05.04_12.45.14.5ccd26ea1b15e.jpg', 'Jl Jaksa Agung Supraptop 39');
INSERT INTO `tb_user` VALUES (2, 'Syantika Wulandari', 'syantika@gmail.com', 2, '089423458234', '2019.05.04_12.53.01.5ccd28bd203b6.jpg', 'Jl Kedawung 91');
INSERT INTO `tb_user` VALUES (3, 'Febrianto Nugroho', 'febri@gmail.com', 1, '081424492846', '2019.05.04_12.53.37.5ccd28e1acecf.jpg', 'Jl Budi Utomo 13');
INSERT INTO `tb_user` VALUES (4, 'Arieq Hertanto', 'arieq@gmail.com', 1, '081199898245', '2019.05.04_12.54.09.5ccd2901b109e.png', 'Jl Melati 191');
INSERT INTO `tb_user` VALUES (5, 'Dinda Mauriza Aulia', 'dinda@gmail.com', 2, '085482034214', '2019.05.04_12.54.36.5ccd291ceb2a7.png', 'Jl Tasikmadu 21');
INSERT INTO `tb_user` VALUES (8, 'Diknas Laraswati', 'diknas@gmail.com', 2, '085582818256', '2019.07.07_03.04.45.5d20fedd448dc.png', 'Jl Mawar Jambe 13');
INSERT INTO `tb_user` VALUES (9, 'Muhammad Waizulkarnain', 'izulluzi@yahoo.com', 1, '', '2019.07.07_03.05.03.5d20feef407e4.png', '');
INSERT INTO `tb_user` VALUES (10, 'Mahsa Alvita', 'mahsa@yahoo.com', 2, '', '2019.07.07_03.05.32.5d20ff0ce865f.png', '');
INSERT INTO `tb_user` VALUES (11, 'Viona Cindy Lala', 'viona@gmail.com', 2, '085728245893', '2019.07.07_03.06.09.5d20ff3117cf2.png', 'Jl Ikan Terbang 245');
INSERT INTO `tb_user` VALUES (12, 'Dylan Prasetyo', 'dylan@gmail.com', 1, '085293585366', '2019.07.07_03.06.51.5d20ff5be835d.png', 'Jl Pattimuran 13');
INSERT INTO `tb_user` VALUES (13, 'Zulfaa Alvita Puspamaya', 'zulfaa@gmail.com', 2, '089282223457', '2019.07.07_03.07.36.5d20ff88e5ccd.png', 'Jl Singosari 234');
INSERT INTO `tb_user` VALUES (14, 'Sugeng Wicaksana', 'sugeng@yahoo.com', 1, '081928484821', '2019.07.07_03.08.19.5d20ffb3c86a2.png', 'Jl Ikan Belida 34');

-- ----------------------------
-- View structure for v_admin_total_head
-- ----------------------------
DROP VIEW IF EXISTS `v_admin_total_head`;
CREATE ALGORITHM = UNDEFINED SQL SECURITY DEFINER VIEW `v_admin_total_head` AS select sum(case when `tb_admin`.`jk` = '1' then 1 else 0 end) AS `total_laki_laki`,sum(case when `tb_admin`.`jk` = '2' then 1 else 0 end) AS `total_perempuan` from `tb_admin` ;

-- ----------------------------
-- View structure for v_user_total_head
-- ----------------------------
DROP VIEW IF EXISTS `v_user_total_head`;
CREATE ALGORITHM = UNDEFINED SQL SECURITY DEFINER VIEW `v_user_total_head` AS select sum(case when `tb_user`.`jk` = '1' then 1 else 0 end) AS `total_laki_laki`,sum(case when `tb_user`.`jk` = '2' then 1 else 0 end) AS `total_perempuan` from `tb_user` ;

SET FOREIGN_KEY_CHECKS = 1;
