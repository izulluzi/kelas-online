<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Data_user extends MY_Controller
{
	function __construct()
	{
		parent::__construct();
		$this->foglobal->CheckSessionLogin();
	}
	public function index(){
		$this->load->view('data_user/data_user.php', array("data_user" => "active"));
	}
}