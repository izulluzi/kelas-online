<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Ajax_data_user extends MY_Controller
{
    public function __construct() {
        parent::__construct();
        $this->load->model("data_user/data_user");
    }
    public function index() {
        $act = $this->input->post("act");
        $form = $this->input->post("form");
        $this->req  = $this->input->post("req");
        $this->form = $this->input->post("form");
        print_r($this->$act());
    }

    private function listdatahtml() {
        $Request = $this->data_user->HtmlDataUser($this->req);
        return $Request;
    }

    private function getdataall() {
        $Request = $this->data_user->GetDataUser($this->req, true);
        return json_encode($Request);
    }

    private function getdatabyid() {
        $Request = $this->data_user->GetDataUser(["filter" => ["id" => $this->req]]);
        return json_encode($Request);
    }

    private function datadropdown() {
        $Request = $this->data_user->DropdownDataUser($this->req);
        return $Request;
    }

    private function insertdata() {
        $Request = $this->data_user->InsertDataUser($this->form);
        return $Request;
    }

    private function updatedata() {
        $id_update = $this->form["id"]; unset($this->form["id"]);
        $Request = $this->data_user->UpdateDataUser($id_update, $this->form);
        return $Request;
    }

    private function deletedata() {
        $id_update = $this->form["id"]; unset($this->form["id"]);
        $Request = $this->data_user->HapusDataUser($id_update);
        return $Request;
    }

    private function total_data_data_user() {
        $Request = $this->data_user->GetDataUserTotalData($this->req);
        return json_encode($Request);
    }
}
