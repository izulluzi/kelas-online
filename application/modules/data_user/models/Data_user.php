<?php
	defined('BASEPATH') OR exit('No direct script access allowed');
	date_default_timezone_set('Asia/Jakarta');

	class Data_user extends CI_Model {

		public function __construct() {
	      	parent::__construct();
	      	$this->load->database();
	      	$this->user = $this->session->userdata("user");
	      	$this->user_login = $this->session->userdata("user_login");
	    }

	    public function GetDataUser($param, $disable_page=false){
			$args["search"] = "";
            $return_data["data"] = "";
            $return_data["paging"]["Count"] = "";
            // START SEARCH & FILTER
			if(!empty($param["Search"])) {
				$this->db->like("nama", $param["Search"]);
				$this->db->or_like("email", $param["Search"]);
				$this->db->or_like("no_telp_hp", $param["Search"]);
				$this->db->or_like("alamat", $param["Search"]);
			}
			if(isset($param["filter"]["jk"]) and $param["filter"]["jk"] != "") {
            	$this->db->where("jk", $param["filter"]["jk"]);
			}
            // END SEARCH & FILTER
            if(!empty($param["Sort"])) {
            	$param["Sort"] = explode(" ",trim($param["Sort"]));
            	$this->db->order_by($param["Sort"][0], $param["Sort"][1]);
			}
	    	if(!empty($param["Limit"])) $args["Limit"] = $param["Limit"];
            if(!empty($param["Page"])) $args["Page"] = $param["Page"];
            if(empty($param["Page"])){
            	$args["Limit"] = "10";
            	$args["Page"] = "1";
            }
            if(empty($param["Limit"])){
            	$args["Limit"] = "10";
            }
            if($disable_page == true){
            	$args["Limit"] = "0";
            	$args["Page"] = "0";
            }
            $this->db->limit($args["Limit"], ($args["Limit"]*$args["Page"])-$args["Limit"]);
			if(!empty($param["filter"]["id"])) {
				$this->db->where("id", $param["filter"]["id"]);
				$return_data["data"] = $this->db->get("tb_user")->result();
			} else {
				$return_data["data"] = $this->db->get("tb_user")->result();
	            // START SEARCH & FILTER 
				if(!empty($param["Search"])) {
					$this->db->like("nama", $param["Search"]);
					$this->db->or_like("email", $param["Search"]);
					$this->db->or_like("no_telp_hp", $param["Search"]);
					$this->db->or_like("alamat", $param["Search"]);
				}
				if(isset($param["filter"]["jk"]) and $param["filter"]["jk"] != "") {
	            	$this->db->where("jk", $param["filter"]["jk"]);
				}
	            // END SEARCH & FILTER
	            $return_data["paging"]["Count"] = count($return_data["data"]);
	            $return_data["paging"]["DataDari"] = ($args["Limit"]*$args["Page"])-$args["Limit"]+1;
	            $return_data["paging"]["DataSampai"] = $return_data["paging"]["DataDari"]+$return_data["paging"]["Count"]-1;
	            $return_data["paging"]["HalKe"] = $args["Page"];
	            $return_data["paging"]["Total"] = $this->db->get("tb_user")->num_rows();
	            $return_data["paging"]["InfoPage"] = $return_data["paging"]["DataDari"]." - ".$return_data["paging"]["DataSampai"]." dari ".$return_data["paging"]["Total"];
	            if($return_data["paging"]["Count"] != 0 && (int)$args["Limit"] != 0){
	            	$return_data["paging"]["JmlHalTotal"] = ceil($return_data["paging"]["Total"]/(int)$args["Limit"]);
	            } else {
	            	$return_data["paging"]["JmlHalTotal"] = 1;
	            }
	            if((int)$return_data["paging"]["HalKe"] < $return_data["paging"]["JmlHalTotal"]){
	            	$return_data["paging"]["IsNext"] = true;
	            } else {
	            	$return_data["paging"]["IsNext"] = false;
	            }
	            $return_data["paging"]["IsPaging"] = true;
	            if((int)$return_data["paging"]["HalKe"] <= 1){
	            	$return_data["paging"]["IsPrev"] = false;
	            } else {
	            	$return_data["paging"]["IsPrev"] = true;
	            }
			}
		    return $return_data;
		}

		public function HtmlDataUser($param){
		    $rHtml ="";
			$query = $this->GetDataUser($param);
			if(!empty($query["data"])) {
				foreach ($query["data"] as $item) {
					$jk = "Laki-Laki";
					if($item->jk == 2){
						$jk = "Perempuan";
					}
                    $rHtml .= "<tr>
								    <td class='text-center'>
								        <div class='btn-group mb-1'>
								            <button class='btn btn-primary btn-sm dropdown-toggle button-menu' type='button' data-toggle='dropdown' aria-haspopup='true' aria-expanded='false'>
								                <i class='fa fa-bars'></i>
								            </button>
								            <div class='dropdown-menu'>
								                <a class='dropdown-item edit-data-user' data-id='".$item->id."'><i class='fa fa-pencil-alt button-icon'></i> Edit</a>
								                <a class='dropdown-item hapus-data-user' data-id='".$item->id."'><i class='fa fa-times button-icon'></i> Hapus</a>
								            </div>
								        </div>
								    </td>
								    <td>".$item->nama."</td>
								    <td>".$item->email."</td>
								    <td>".$jk."</td>
								    <td>".$item->no_telp_hp."</td>
								    <td>".$item->alamat."</td>
								</tr>";

				}
			} else {
				$rHtml = "<tr><td colspan='6' class='text-center'><span class='badge badge-pill badge-warning'>Tidak ada data</span></td></tr>";
			}
			$Paging = (!empty($query["paging"])) ? $query["paging"] : "";
			$ReturnData = ["lsdt" => $rHtml, "paging" => $Paging];
            return json_encode($ReturnData);
		}

		public function DropdownDataUser($param) {
			$rHtml ="";
			$query = $this->GetDataUser($param, true);
			if(!empty($query["data"])) {
				foreach ($query["data"] as $item) {
					 $rHtml .= '<option value="'.$item->id.'">'.$item->nama.'</option>';
				}
			} else {
				$rHtml = '<option disabled value="">Tidak Ada Data</option>';
			}
            $ReturnData = ["lsdt" => $rHtml];
            return json_encode($ReturnData);
		}

		public function InsertDataUser($args) {
			$query = $this->db->insert("tb_user", $args);
			if(!$query){
			   	$return_data["IsError"] = true;
				$return_data["ErrorMessage"] = $this->db->conn_id->error_list;
				return json_encode($return_data);
			}
		   	return $query;
        }

        public function UpdateDataUser($id_update, $param) {
        	if(isset($param["session"])) {
        		unset($param["session"]);
			}
         	$this->db->where("id", $id_update);
		    $query = $this->db->update("tb_user", $param);
		    if(!$query){
			   	$return_data["IsError"] = true;
				$return_data["ErrorMessage"] = $this->db->conn_id->error_list;
				return json_encode($return_data);
			}
		   	return $query;
        }

        public function HapusDataUser($id_update) {
		    $this->db->where("id", $id_update);
		    $query = $this->db->delete("tb_user");
		    if(!$query){
			   	$return_data["IsError"] = true;
				$return_data["ErrorMessage"] = $this->db->conn_id->error_list;
				return json_encode($return_data);
			}
		   	return $query;
        }


        public function GetDataUserTotalData($param){
		    $query = $this->db->get("v_user_total_head")->result();
		    if(!$query){
			   	$return_data["IsError"] = true;
				$return_data["ErrorMessage"] = $this->db->conn_id->error_list;
				return json_encode($return_data);
			}
		   	return $query;
		}
	}
