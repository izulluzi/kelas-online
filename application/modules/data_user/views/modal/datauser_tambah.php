<div class="modal fade" id="modal-tambah-data-user" role="dialog" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header bg-primary">
                <h5 class="modal-title text-white">Tambah User</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true" style="color: white;">&times;</span>
                </button>
            </div>
            <div class="modal-body bg-secondary">
                <form id="FrmTambahUser" class="form-horizontal form-data-user-baru" role="form" method="POST" action="/data_user/ajax_data_user">
                    <div class="form-group">
                        <label class="form-control-label">Foto</label>
                        <input type="file" class="form-control form-control-alternative foto" placeholder="Foto 1">
                        <br class="strip-foto d-none">
                        <label class="loading-foto d-none"><i class="fa fa-spin fa-sync-alt"></i> </label><button style="margin:2px;" type="button" class="btn btn-sm btn-primary detail-foto d-none"><i class="fa fa-external-link-alt"></i> Lihat File</button><button style="margin:2px;" type="button" class="btn btn-sm btn-danger hapus-foto d-none"><i class="fa fa-trash"></i> Hapus File</button>
                        <input class="foto_hidden" type="hidden" name="form[foto]" value="">
                    </div>
                    <div class="form-group">
                        <label class="form-control-label">Nama <span class="text-danger">*</span></label>
                        <input required type="text" class="form-control form-control-alternative" placeholder="Nama" name="form[nama]" maxlength="255">
                    </div>
                    <div class="form-group">
                        <label class="form-control-label">Jenis Kelamin <span class="text-danger">*</span></label>
                        <select required class="form-control form-control-alternative select" name="form[jk]">
                            <option value="1">Laki-Laki</option>
                            <option value="2">Perempuan</option>
                        </select>
                    </div>
                    <div class="form-group">
                        <label class="form-control-label">Email <span class="text-danger">*</span></label>
                        <input required type="text" class="form-control form-control-alternative" placeholder="Email" name="form[email]" maxlength="255">
                    </div>
                    <div class="form-group">
                        <label class="form-control-label">No Telp HP</label>
                        <input type="text" class="form-control form-control-alternative" placeholder="No Telp HP" name="form[no_telp_hp]" maxlength="20">
                    </div>
                    <div class="form-group">
                        <label class="form-control-label">Alamat</label>
                        <input type="alamat" class="form-control form-control-alternative" placeholder="Alamat" name="form[alamat]" maxlength="255">
                    </div>
                </form>
            </div>
            <div class="modal-footer bg-primary">
                <button class="btn btn-light" data-dismiss="modal">Batal</button>
                <button type="submit" class="btn btn-neutral ladda-button ladda-button-submit" onclick="SimpanTambahUser();" data-style="slide-up">Simpan</button>
            </div>
        </div>
    </div>
</div>