<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <meta name="description" content="Start your development with a Dashboard for Bootstrap 4.">
        <meta name="author" content="Creative Tim">
        <title>User</title>
        <link href="<?php echo base_url("assets/img/brand/favicon.png"); ?>" rel="icon" type="image/png">
        <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700" rel="stylesheet">
        <link href="<?php echo base_url("assets/vendor/nucleo/css/nucleo.css"); ?>" rel="stylesheet">
        <link href="<?php echo base_url("assets/vendor/@fortawesome/fontawesome-free/css/all.min.css"); ?>" rel="stylesheet">
        <link href="<?php echo base_url("assets/plugins/select2/select2.min.css"); ?>" rel="stylesheet">
        <link href="<?php echo base_url("assets/plugins/select2/select2-bootstrap.min.css"); ?>" rel="stylesheet">
        <link href="<?php echo base_url("assets/plugins/ladda/ladda-themeless.min.css"); ?>" rel="stylesheet">
        <link href="<?php echo base_url("assets/plugins/toastr/toastr.min.css"); ?>" rel="stylesheet">
        <link href="<?php echo base_url("assets/css/bootstrap-datepicker.min.css"); ?>" rel="stylesheet">
        <link href="<?php echo base_url("assets/css/argon.css?v=1.0.0"); ?>" rel="stylesheet">
        <link href="<?php echo base_url("assets/css/custom.css"); ?>" rel="stylesheet">
    </head>
    <body>
    <?php $this->load->view("other/sidebar"); ?>
    <div class="main-content">
        <?php $this->load->view("other/header"); ?>
        <div class="header bg-primary pb-7">
            <div class="container-fluid">
                <div class="header-body">
                    <div class="row align-items-center head-padding-setup">
                        <div class="col-lg-4 col-md-6">
                            <nav aria-label="breadcrumb">
                                <ol class="breadcrumb breadcrumb-links breadcrumb-dark">
                                    <li class="breadcrumb-item"><i class="fa fa-user"></i></a></li>
                                    <li class="breadcrumb-item">User</li>
                                    <li class="breadcrumb-item active" aria-current="page">Daftar User</li>
                                </ol>
                            </nav>
                        </div>
                        <div class="col-lg-8 col-md-6 text-right">
                            <div class="form-group float-right div-header-button">
                                <button type="button" class="btn btn-sm btn-neutral" data-toggle="modal" data-target="#modal-tambah-data-user" title="Tambah Data"><i class="fa fa-plus"></i></button>
                                <button type="button" class="btn btn-sm btn-neutral" data-toggle="collapse" data-target=".filter" title="Filter Data"><i class="fa fa-filter"></i></button>
                                <button type="button" class="btn btn-sm btn-neutral" onclick="location.reload();"><i class="fa fa-sync-alt" title="Refresh"></i></button>
                                <div class="btn-group pagination-layout-data-user" pagename="/data_user/ajax_data_user" data-colspan="6" role="group" aria-label="Basic example">
                                    <button type="button" disabled class="btn btn-sm btn-neutral disabled prev-head"><i class="fa fa-chevron-left"></i></button>
                                    <button type="button" disabled class="btn btn-sm btn-neutral disabled next-head"><i class="fa fa-chevron-right"></i></button>
                                </div>
                            </div>
                            <div class="form-group float-right div-header-search">
                                <form id="FrmSearch">
                                    <div class="input-group input-search">
                                        <input type="text" class="form-control kywd" placeholder="Cari (Nama, Email, No Telp HP, Alamat)" title="Cari (Nama, Email, No Telp HP, Alamat)" aria-describedby="basic-addon2">
                                        <div class="input-group-append">
                                            <button class="btn btn-neutral btn-sm" type="submit"><i class="fa fa-search"></i></button>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                    <div class="row data-head-data-user">
                        <div class="col-xl-6 col-lg-6">
                            <div class="card card-stats mb-4 mb-xl-0">
                                <div class="card-body">
                                    <div class="row">
                                        <div class="col">
                                            <h5 class="card-title text-uppercase text-muted mb-0">Laki-Laki</h5>
                                            <span class="h2 font-weight-bold mb-0 total_laki_laki">0</span>
                                        </div>
                                        <div class="col-auto">
                                            <div class="icon icon-shape bg-primary text-white rounded-circle shadow">
                                                <i class="fa fa-male"></i>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-xl-6 col-lg-6">
                            <div class="card card-stats mb-4 mb-xl-0">
                                <div class="card-body">
                                    <div class="row">
                                        <div class="col">
                                            <h5 class="card-title text-uppercase text-muted mb-0">Perempuan</h5>
                                            <span class="h2 font-weight-bold mb-0 total_perempuan">0</span>
                                        </div>
                                        <div class="col-auto">
                                            <div class="icon icon-shape bg-danger text-white rounded-circle shadow">
                                                <i class="fa fa-female"></i>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="container-fluid mt--6">
            <div class="row">
                <div class="col">
                    <div class="card shadow">
                        <div class="card-body filter collapse">
                            <form id="FrmFilter" role="form">
                                <div class="panel-body row">
                                    <div class="col-md-3 col-sm-6">
                                        <div class="form-group">
                                            <label>Jenis Kelamin</label>
                                            <select style="width: 100%;" class="form-control select jk">
                                                <option value="">Semua</option>
                                                <option value="1">Laki-Laki</option>
                                                <option value="2">Perempuan</option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-md-3 col-sm-6">
                                        <div class="form-group">
                                            <label>Urutkan</label>
                                            <select style="width: 100%;" class="form-control select sort">
                                                <option value="id desc">Default</option>
                                                <option value="nama asc">Nama [A-Z]</option>
                                                <option value="nama desc">Nama [Z-A]</option>
                                                <option value="email asc">Email [A-Z]</option>
                                                <option value="email desc">Email [Z-A]</option>
                                                <option value="no_telp_hp asc">No Telp HP [A-Z]</option>
                                                <option value="no_telp_hp desc">No Telp HP [Z-A]</option>
                                                <option value="alamat asc">Alamat [A-Z]</option>
                                                <option value="alamat desc">Alamat [Z-A]</option>
                                            </select>
                                        </div>
                                    </div>
                                </div>
                            </form>
                        </div>
                        <div class="table-responsive">
                            <table class="table datatable-data-user table-striped table-bordered">
                                <thead class="thead-light">
                                    <tr>
                                        <th style="width: 80px;" class="text-center">Aksi</th>
                                        <th>Nama</th>
                                        <th>Email</th>
                                        <th>Jenis Kelamin</th>
                                        <th>No Telp HP</th>
                                        <th>Alamat</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr>
                                        <td colspan="6">
                                            <center>
                                                <img class="loading-gif-image" src="<?php echo base_url("assets/img/loading-data.gif") ?>" alt="Loading ...">
                                            </center>
                                        </td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                        <div class="card-footer div-pagination-bottom">
                            <div class="pagination-layout-data-user d-none" pagename="/data_user/ajax_data_user" data-colspan="6">
                                <div class="row">
                                    <div class="col-md-4 col-xs-8">
                                        <form id="FrmGotoPage">
                                            <div class="input-group">
                                                <div class="input-group-prepend">
                                                    <button type="button" disabled class="btn btn-primary disabled first"><i class="fa fa-step-backward"></i></button>
                                                    <button type="button" disabled class="btn btn-primary disabled prev"><i class="fa fa-chevron-left"></i></button>
                                                </div>
                                                <input type="text" class="form-control text-center" aria-describedby="basic-addon2" onkeypress="return validatedata(event);" value="1">
                                                <div class="input-group-append">
                                                    <button type="button" disabled class="btn btn-primary disabled next"><i class="fa fa-chevron-right"></i></button>
                                                    <button type="button" disabled class="btn btn-primary disabled last"><i class="fa fa-step-forward"></i></button>
                                                </div>
                                            </div>
                                        </form>
                                    </div>
                                    <div class="col-sm-6 d-none d-sm-none d-sm-none d-lg-block">
                                        <div class="form-group">
                                            <div class="info text-right info-paging">1 - 10 dari 100 Data | 1 Halaman</div>
                                        </div>
                                    </div>
                                    <div class="col-sm-2 d-none d-sm-none d-sm-none d-lg-block">
                                        <div class="form-group">
                                            <select class="form-control select limit float-right">
                                                <option value="10" selected>10</option>
                                                <option value="20">20</option>
                                                <option value="30">30</option>
                                                <option value="40">40</option>
                                                <option value="50">50</option>
                                                <option value="100">100</option>
                                            </select>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <?php $this->load->view("other/footer"); ?>
        </div>
        <?php $this->load->view("data_user/modal/datauser_tambah"); ?>
        <?php $this->load->view("data_user/modal/datauser_edit"); ?>
        <?php $this->load->view("data_user/modal/datauser_hapus"); ?>
        <script src="<?php echo base_url("assets/vendor/jquery/dist/jquery.min.js"); ?>"></script>
        <script src="<?php echo base_url("assets/vendor/bootstrap/dist/js/bootstrap.bundle.min.js"); ?>"></script>
        <script src="<?php echo base_url("assets/js/argon.js?v=1.0.0"); ?>"></script>
        <script src="<?php echo base_url("assets/js/bootstrap-datepicker.js"); ?>"></script>
        <script src="<?php echo base_url("assets/plugins/validate/jquery.validate.min.js"); ?>"></script>
        <script src="<?php echo base_url("assets/plugins/select2/select2.full.js"); ?>"></script>
        <script src="<?php echo base_url("assets/plugins/toastr/toastr.min.js"); ?>"></script>
        <script src="<?php echo base_url("assets/plugins/ladda/spin.min.js"); ?>"></script>
        <script src="<?php echo base_url("assets/plugins/ladda/ladda.min.js"); ?>"></script>
        <script src="<?php echo base_url("assets/plugins/ladda/ladda.jquery.min.js"); ?>"></script>
        <script src="<?php echo base_url("assets/js/moment.min.js"); ?>"></script>
        <script src="<?php echo base_url("assets/js/proses.js"); ?>"></script>
        <script>
            var post_gambar  = {"form": {"Base64": ""}};
            
            $(document).ready(function(){
                GetDataHead();
                GetDataTable();
            });

            function GetDataTable(){
                var kywd = $("#FrmSearch").find(".kywd").val(), jk = $("#FrmFilter").find(".jk").val(), status = $("#FrmFilter").find(".status").val(), sort = $("#FrmFilter").find(".sort").val();
                request["Page"] = 1;
                request["Sort"] = sort;
                request["Search"] = kywd;
                request["filter"]["jk"] = jk;
                pagename = "/data_user/ajax_data_user";
                target_table = "data-user";
                GetData(request,"listdatahtml",target_table,function(resp){

                }, 6);
                return false;
            }

            $("#FrmSearch").submit(function() {
                GetDataTable();
                return false;
            });
            $("#FrmFilter .jk, #FrmFilter .sort").change(function(){
                GetDataTable();
            });

            function SimpanTambahUser(){
                $("#FrmTambahUser").submit();
            }
            function SimpanEditUser(){
                $("#FrmEditUser").submit();
            }

            var FrmTambahUser = $("#FrmTambahUser").validate({
                submitHandler: function(form) {
                    laddasubmit = data_user_baru.find(".ladda-button-submit");
                    InsertData(form, function(resp) {
                        GetDataTable();
                    });
                },
                errorPlacement: function (error, element) {
                    if (element.parent(".input-group").length) { 
                        error.insertAfter(element.parent());      // radio/checkbox?
                    } else if (element.hasClass("select2") || element.hasClass("select")) {
                        error.insertAfter(element.next("span"));  // select2
                    } else {                                      
                        error.insertAfter(element);               // default
                    }
                }
            });
            var FrmEditUser = $("#FrmEditUser").validate({
                submitHandler: function(form) {
                    laddasubmit = data_user_edit.find(".ladda-button-submit");
                    UpdateData(form, function(resp) {
                        GetDataTable();
                    });
                },
                errorPlacement: function (error, element) {
                    if (element.parent(".input-group").length) { 
                        error.insertAfter(element.parent());      // radio/checkbox?
                    } else if (element.hasClass("select2") || element.hasClass("select")) {
                        error.insertAfter(element.next("span"));  // select2
                    } else {                                      
                        error.insertAfter(element);               // default
                    }
                }
            });
            var FrmHapusUser = $("#FrmHapusUser").validate({
                submitHandler: function(form) {
                    laddasubmit = data_user_hapus.find(".ladda-button-submit");
                    DeleteData(form, function(resp) {
                        GetDataTable();
                    });
                },
                errorPlacement: function (error, element) {
                    if (element.parent(".input-group").length) { 
                        error.insertAfter(element.parent());      // radio/checkbox?
                    } else if (element.hasClass("select2") || element.hasClass("select")) {
                        error.insertAfter(element.next("span"));  // select2
                    } else {                                      
                        error.insertAfter(element);               // default
                    }
                }
            });

            function GetDataHead(){
                $.ajax({
                    type: "POST",
                    url: base_url + "/data_user/ajax_data_user",
                    data: {act:"total_data_data_user", req:request},
                    dataType: "JSON",
                    tryCount: 0,
                    retryLimit: 3,
                    beforeSend: function() {
                        $(".data-head-data-user").find(".total_laki_laki, .total_perempuan").html("<i class='fa fa-spin fa-sync-alt'></a>");
                    },
                    success: function(resp){
                        var resp = resp[0];
                        if(resp.IsError != undefined) {
                            if(resp.ErrorMessage[0].error != undefined) {
                                $(".data-head-data-user").find(".total_laki_laki, .total_perempuan").html("<span style='font-size: 10px;'>"+resp.ErrorMessage[0].error+"</span>");
                            } else {
                                $(".data-head-data-user").find(".total_laki_laki, .total_perempuan").html("<span style='font-size: 10px;'>"+resp.ErrorMessager+"</span>");
                            }
                        } else {
                            $(".data-head-data-user").find(".total_laki_laki").html("<strong>"+rupiah(resp.total_laki_laki)+"</strong>");
                            $(".data-head-data-user").find(".total_perempuan").html("<strong>"+rupiah(resp.total_perempuan)+"</strong>");
                        }
                    },
                    error: function(xhr, textstatus, errorthrown) {
                        $(".data-head-pembelian").find(".total_laki_laki, .total_perempuan").html("<span style='font-size: 10px;'>Periksa koneksi internet anda kembali</span>");
                    }
                });
            }


            var data_user_baru = $("#modal-tambah-data-user");
            var data_user_edit = $("#modal-edit-data-user");
            var data_user_hapus = $("#modal-hapus-data-user");
            $("#modal-tambah-data-user").on('show.bs.modal', function () {
                if(($("#modal-tambah-data-user").data('bs.modal') || {})._isShown){

                } else {
                    data_user_baru.find("input[name='form[nama]'], input[name='form[email]'], input[name='form[no_telp_hp]'], input[name='form[alamat]']").val("");
                    data_user_baru.find(".hapus-foto").click();
                    $("#modal-tambah-data-user").on('shown.bs.modal', function () {
                        data_user_baru.find("input[name='form[nama]']").focus();
                    });
                }
                
            });
            $(".datatable-data-user").on("click", ".edit-data-user", function() {
                $("#modal-edit-data-user .modal-title").text("Edit User : Nama User");
                data_user_edit.find(".loading-gif-image").removeClass("d-none");
                data_user_edit.find(".after-loading").addClass("d-none");
                $("#modal-edit-data-user").modal("show");
                var id_update = $(this).attr("data-id");
                data_user_edit.find(".id_hidden").val(id_update);
                pagename = "/data_user/ajax_data_user";
                GetDataById(id_update, function(resp) {
                    var resp = resp.data[0];
                    data_user_edit.find("input[name='form[nama]']").val(resp.nama);
                    data_user_edit.find("input[name='form[email]']").val(resp.email);
                    data_user_edit.find("input[name='form[no_telp_hp]']").val(resp.no_telp_hp);
                    data_user_edit.find("input[name='form[alamat]']").val(resp.alamat);
                    data_user_edit.find("select[name='form[jk]']").val(resp.jk);
                    if(resp.foto == "" || resp.foto == null){
                        data_user_edit.find(".detail-foto, .strip-foto, .hapus-foto").addClass("d-none");
                        data_user_edit.find(".foto").removeClass("d-none");
                    } else {
                        data_user_edit.find(".detail-foto, .strip-foto, .hapus-foto").removeClass("d-none");
                        data_user_edit.find(".foto, .loading-foto").addClass("d-none");
                    }
                    data_user_edit.find("input[name='form[foto]']").val(resp.foto);
                    data_user_edit.find(".loading-gif-image").addClass("d-none");
                    data_user_edit.find(".after-loading").removeClass("d-none");
                    data_user_edit.find("input[name='form[nama]']").focus();
                    $("#modal-edit-data-user .modal-title").text("Edit User : " + resp.nama);
                });
            });
            $(".datatable-data-user").on("click", ".hapus-data-user", function() {
                $("#modal-hapus-data-user .modal-title").text("Hapus User : Nama User");
                data_user_hapus.find(".loading-gif-image").removeClass("d-none");
                data_user_hapus.find(".after-loading").addClass("d-none");
                $("#modal-hapus-data-user").modal("show");
                var id_update = $(this).attr("data-id");
                data_user_hapus.find(".id_hidden").val(id_update);
                pagename = "/data_user/ajax_data_user";
                GetDataById(id_update, function(resp) {
                    var resp = resp.data[0];
                    data_user_hapus.find(".loading-gif-image").addClass("d-none");
                    data_user_hapus.find(".after-loading").removeClass("d-none");
                    $("#modal-hapus-data-user .modal-title").text("Hapus User : " + resp.nama);
                });
            });

            data_user_baru.find(".foto").change(function() {
                var selector = $(this);
                if (this.files && this.files[0]) {
                    var tipefile = this.files[0].type.toString();
                    var filename = this.files[0].name.toString();
                    var tipe = ['image/png',  'image/x-png', 'image/jpeg', 'image/pjpeg'];
                    if(tipe.indexOf(tipefile) == -1) {
                        $(this).val("");
                        toastrshow("warning", "Format salah, pilih file dengan format png/jpg/jpeg", "Peringatan");
                        return;
                    }
                    if((this.files[0].size / 1024) < 2048){
                        var FR = new FileReader();
                        FR.addEventListener("load", function(e) {
                            //var base64 = e.target.result;
                            var base64 = e.target.result.replace("data:"+tipefile+";base64,", '');
                            var ext = filename.split(".").pop();
                            post_gambar["form"]["Base64"] = base64;
                            post_gambar["form"]["Ext"] = ext;
                            console.log(post_gambar);
                            $.ajax({
                                type: "POST",
                                url: base_url + "/tool/ajax_tool",
                                data: {act:"upload_file", form:post_gambar},
                                dataType: "JSON",
                                tryCount: 0,
                                retryLimit: 3,
                                beforeSend: function(){
                                    data_user_baru.find(".foto_hidden").val("");
                                    data_user_baru.find(".foto").addClass("d-none");
                                    data_user_baru.find(".loading-foto").removeClass("d-none");
                                },
                                success: function(respon_file){
                                    if(respon_file.IsError != undefined) {
                                        if(respon_file.ErrorMessage[0].error != undefined) {
                                            toastrshow("warning", respon_file.ErrorMessage[0].error, "Peringatan");
                                        } else {
                                            toastrshow("warning", respon_file.ErrorMessage, "Peringatan");
                                        }
                                    } else {
                                        data_user_baru.find(".foto_hidden").val(respon_file.Output);
                                        data_user_baru.find(".detail-foto, .strip-foto, .hapus-foto").removeClass("d-none");
                                        data_user_baru.find(".foto").addClass("d-none");
                                        data_user_baru.find(".loading-foto").addClass("d-none");
                                    }
                                },
                                error: function(xhr, textstatus, errorthrown) {
                                    toastrshow("warning", "Periksa koneksi internet anda kembali", "Peringatan");
                                    data_user_baru.find(".hapus-foto").click();
                                    data_user_baru.find(".loading-foto").addClass("d-none");
                                }
                            });
                        }); 
                        FR.readAsDataURL(this.files[0]);
                    } else {
                        data_user_baru.find(this).val("");
                        toastrshow("warning", "Ukuran file maksimum adalah 2 MB", "Warning");
                    }
                }
            });
            data_user_baru.find(".hapus-foto").click(function(){
                data_user_baru.find(".foto_hidden").val("");
                data_user_baru.find(".detail-foto, .strip-foto, .hapus-foto").addClass("d-none");
                data_user_baru.find(".foto").val("").trigger("change");
                data_user_baru.find(".foto").removeClass("d-none");
            });
            data_user_baru.find(".detail-foto").click(function(){
                window.open("<?php echo base_url("assets/cdn/"); ?>"+data_user_baru.find(".foto_hidden").val(), '_blank');
            });
            data_user_edit.find(".foto").change(function() {
                var selector = $(this);
                if (this.files && this.files[0]) {
                    var tipefile = this.files[0].type.toString();
                    var filename = this.files[0].name.toString();
                    var tipe = ['image/png',  'image/x-png', 'image/jpeg', 'image/pjpeg'];
                    if(tipe.indexOf(tipefile) == -1) {
                        $(this).val("");
                        toastrshow("warning", "Format salah, pilih file dengan format png/jpg/jpeg", "Peringatan");
                        return;
                    }
                    if((this.files[0].size / 1024) < 2048){
                        var FR = new FileReader();
                        FR.addEventListener("load", function(e) {
                            //var base64 = e.target.result;
                            var base64 = e.target.result.replace("data:"+tipefile+";base64,", '');
                            var ext = filename.split(".").pop();
                            post_gambar["form"]["Base64"] = base64;
                            post_gambar["form"]["Ext"] = ext;
                            console.log(post_gambar);
                            $.ajax({
                                type: "POST",
                                url: base_url + "/tool/ajax_tool",
                                data: {act:"upload_file", form:post_gambar},
                                dataType: "JSON",
                                tryCount: 0,
                                retryLimit: 3,
                                beforeSend: function(){
                                    data_user_edit.find(".foto_hidden").val("");
                                    data_user_edit.find(".foto").addClass("d-none");
                                    data_user_edit.find(".loading-foto").removeClass("d-none");
                                },
                                success: function(respon_file){
                                    if(respon_file.IsError != undefined) {
                                        if(respon_file.ErrorMessage[0].error != undefined) {
                                            toastrshow("warning", respon_file.ErrorMessage[0].error, "Peringatan");
                                        } else {
                                            toastrshow("warning", respon_file.ErrorMessage, "Peringatan");
                                        }
                                    } else {
                                        data_user_edit.find(".foto_hidden").val(respon_file.Output);
                                        data_user_edit.find(".detail-foto, .strip-foto, .hapus-foto").removeClass("d-none");
                                        data_user_edit.find(".foto").addClass("d-none");
                                        data_user_edit.find(".loading-foto").addClass("d-none");
                                    }
                                },
                                error: function(xhr, textstatus, errorthrown) {
                                    toastrshow("warning", "Periksa koneksi internet anda kembali", "Peringatan");
                                    data_user_edit.find(".hapus-foto").click();
                                    data_user_edit.find(".loading-foto").addClass("d-none");
                                }
                            });
                        }); 
                        FR.readAsDataURL(this.files[0]);
                    } else {
                        data_user_edit.find(this).val("");
                        toastrshow("warning", "Ukuran file maksimum adalah 2 MB", "Warning");
                    }
                }
            });
            data_user_edit.find(".hapus-foto").click(function(){
                data_user_edit.find(".foto_hidden").val("");
                data_user_edit.find(".detail-foto, .strip-foto, .hapus-foto").addClass("d-none");
                data_user_edit.find(".foto").val("").trigger("change");
                data_user_edit.find(".foto").removeClass("d-none");
            });
            data_user_edit.find(".detail-foto").click(function(){
                window.open("<?php echo base_url("assets/cdn/"); ?>"+data_user_edit.find(".foto_hidden").val(), '_blank');
            });
        </script>
    </body>
</html>