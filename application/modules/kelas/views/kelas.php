<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <meta name="description" content="Start your development with a Dashboard for Bootstrap 4.">
        <meta name="author" content="Creative Tim">
        <title>Kelas</title>
        <link href="<?php echo base_url("assets/img/brand/favicon.png"); ?>" rel="icon" type="image/png">
        <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700" rel="stylesheet">
        <link href="<?php echo base_url("assets/vendor/nucleo/css/nucleo.css"); ?>" rel="stylesheet">
        <link href="<?php echo base_url("assets/vendor/@fortawesome/fontawesome-free/css/all.min.css"); ?>" rel="stylesheet">
        <link href="<?php echo base_url("assets/plugins/select2/select2.min.css"); ?>" rel="stylesheet">
        <link href="<?php echo base_url("assets/plugins/select2/select2-bootstrap.min.css"); ?>" rel="stylesheet">
        <link href="<?php echo base_url("assets/plugins/ladda/ladda-themeless.min.css"); ?>" rel="stylesheet">
        <link href="<?php echo base_url("assets/plugins/toastr/toastr.min.css"); ?>" rel="stylesheet">
        <link href="<?php echo base_url("assets/css/bootstrap-datepicker.min.css"); ?>" rel="stylesheet">
        <link href="<?php echo base_url("assets/css/argon.css?v=1.0.0"); ?>" rel="stylesheet">
        <link href="<?php echo base_url("assets/css/custom.css"); ?>" rel="stylesheet">
        <style>
            .nopad {
                padding-left: 0 !important;
                padding-right: 0 !important;
            }
            /*image gallery*/
            .image-checkbox {
                cursor: pointer;
                box-sizing: border-box;
                -moz-box-sizing: border-box;
                -webkit-box-sizing: border-box;
                border: 4px solid transparent;
                margin-bottom: 0;
                outline: 0;
            }
            .image-checkbox input[type="checkbox"] {
                display: none;
            }

            .image-checkbox-checked {
                border-color: #4783B0;
            }
            .image-checkbox .fa {
              position: absolute;
              color: #4A79A3;
              background-color: #fff;
              padding: 10px;
              top: 0;
              right: 0;
            }
            .image-checkbox-checked .fa {
              display: block !important;
            }
            .data-user label img{
                width: 100%;
                height: auto;
            }
            .label-14{
                margin-bottom: 0px;
                font-size: 12px;
                line-height: 0px;
                text-align: left;
            }
        </style>
    </head>
    <body>
    <?php $this->load->view("other/sidebar"); ?>
    <div class="main-content">
        <?php $this->load->view("other/header"); ?>
        <div class="header bg-primary pb-7">
            <div class="container-fluid">
                <div class="header-body">
                    <div class="row align-items-center head-padding-setup">
                        <div class="col-lg-4 col-md-6">
                            <nav aria-label="breadcrumb">
                                <ol class="breadcrumb breadcrumb-links breadcrumb-dark">
                                    <li class="breadcrumb-item"><i class="fa fa-layer-group"></i></a></li>
                                    <li class="breadcrumb-item">Kelas</li>
                                    <li class="breadcrumb-item active" aria-current="page">Daftar Kelas</li>
                                </ol>
                            </nav>
                        </div>
                        <div class="col-lg-8 col-md-6 text-right">
                            <div class="form-group float-right div-header-button">
                                <button type="button" class="btn btn-sm btn-neutral" data-toggle="modal" data-target="#modal-tambah-kelas" title="Tambah Data"><i class="fa fa-plus"></i></button>
                                <button type="button" class="btn btn-sm btn-neutral" data-toggle="collapse" data-target=".filter" title="Filter Data"><i class="fa fa-filter"></i></button>
                                <button type="button" class="btn btn-sm btn-neutral" onclick="location.reload();"><i class="fa fa-sync-alt" title="Refresh"></i></button>
                                <div class="btn-group pagination-layout-kelas" pagename="/kelas/ajax_kelas" data-colspan="6" role="group" aria-label="Basic example">
                                    <button type="button" disabled class="btn btn-sm btn-neutral disabled prev-head"><i class="fa fa-chevron-left"></i></button>
                                    <button type="button" disabled class="btn btn-sm btn-neutral disabled next-head"><i class="fa fa-chevron-right"></i></button>
                                </div>
                            </div>
                            <div class="form-group float-right div-header-search">
                                <form id="FrmSearch">
                                    <div class="input-group input-search">
                                        <input type="text" class="form-control kywd" placeholder="Cari (Nama, Keterangan)" title="Cari (Nama, Keterangan)" aria-describedby="basic-addon2">
                                        <div class="input-group-append">
                                            <button class="btn btn-neutral btn-sm" type="submit"><i class="fa fa-search"></i></button>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="container-fluid mt--6">
            <div class="row">
                <div class="col">
                    <div class="card shadow">
                        <div class="card-body filter collapse">
                            <form id="FrmFilter" role="form">
                                <div class="panel-body row">
                                    <div class="col-md-3 col-sm-6">
                                        <div class="form-group">
                                            <label>Urutkan</label>
                                            <select style="width: 100%;" class="form-control select sort">
                                                <option value="id desc">Default</option>
                                                <option value="nama asc">Nama [A-Z]</option>
                                                <option value="nama desc">Nama [Z-A]</option>
                                                <option value="keterangan asc">Keterangan [A-Z]</option>
                                                <option value="keterangan desc">Keterangan [Z-A]</option>
                                            </select>
                                        </div>
                                    </div>
                                </div>
                            </form>
                        </div>
                        <div class="table-responsive">
                            <table class="table datatable-kelas table-striped table-bordered">
                                <thead class="thead-light">
                                    <tr>
                                        <th style="width: 80px;" class="text-center">Aksi</th>
                                        <th>Nama</th>
                                        <th>Keterangan</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr>
                                        <td colspan="3">
                                            <center>
                                                <img class="loading-gif-image" src="<?php echo base_url("assets/img/loading-data.gif") ?>" alt="Loading ...">
                                            </center>
                                        </td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                        <div class="card-footer div-pagination-bottom">
                            <div class="pagination-layout-kelas d-none" pagename="/kelas/ajax_kelas" data-colspan="6">
                                <div class="row">
                                    <div class="col-md-4 col-xs-8">
                                        <form id="FrmGotoPage">
                                            <div class="input-group">
                                                <div class="input-group-prepend">
                                                    <button type="button" disabled class="btn btn-primary disabled first"><i class="fa fa-step-backward"></i></button>
                                                    <button type="button" disabled class="btn btn-primary disabled prev"><i class="fa fa-chevron-left"></i></button>
                                                </div>
                                                <input type="text" class="form-control text-center" aria-describedby="basic-addon2" onkeypress="return validatedata(event);" value="1">
                                                <div class="input-group-append">
                                                    <button type="button" disabled class="btn btn-primary disabled next"><i class="fa fa-chevron-right"></i></button>
                                                    <button type="button" disabled class="btn btn-primary disabled last"><i class="fa fa-step-forward"></i></button>
                                                </div>
                                            </div>
                                        </form>
                                    </div>
                                    <div class="col-sm-6 d-none d-sm-none d-sm-none d-lg-block">
                                        <div class="form-group">
                                            <div class="info text-right info-paging">1 - 10 dari 100 Data | 1 Halaman</div>
                                        </div>
                                    </div>
                                    <div class="col-sm-2 d-none d-sm-none d-sm-none d-lg-block">
                                        <div class="form-group">
                                            <select class="form-control select limit float-right">
                                                <option value="10" selected>10</option>
                                                <option value="20">20</option>
                                                <option value="30">30</option>
                                                <option value="40">40</option>
                                                <option value="50">50</option>
                                                <option value="100">100</option>
                                            </select>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <?php $this->load->view("other/footer"); ?>
        </div>
        <?php $this->load->view("kelas/modal/kelas_detail"); ?>
        <?php $this->load->view("kelas/modal/kelas_tambah"); ?>
        <?php $this->load->view("kelas/modal/kelas_edit"); ?>
        <?php $this->load->view("kelas/modal/kelas_hapus"); ?>
        <script src="<?php echo base_url("assets/vendor/jquery/dist/jquery.min.js"); ?>"></script>
        <script src="<?php echo base_url("assets/vendor/bootstrap/dist/js/bootstrap.bundle.min.js"); ?>"></script>
        <script src="<?php echo base_url("assets/js/argon.js?v=1.0.0"); ?>"></script>
        <script src="<?php echo base_url("assets/js/bootstrap-datepicker.js"); ?>"></script>
        <script src="<?php echo base_url("assets/plugins/validate/jquery.validate.min.js"); ?>"></script>
        <script src="<?php echo base_url("assets/plugins/select2/select2.full.js"); ?>"></script>
        <script src="<?php echo base_url("assets/plugins/toastr/toastr.min.js"); ?>"></script>
        <script src="<?php echo base_url("assets/plugins/ladda/spin.min.js"); ?>"></script>
        <script src="<?php echo base_url("assets/plugins/ladda/ladda.min.js"); ?>"></script>
        <script src="<?php echo base_url("assets/plugins/ladda/ladda.jquery.min.js"); ?>"></script>
        <script src="<?php echo base_url("assets/js/moment.min.js"); ?>"></script>
        <script src="<?php echo base_url("assets/js/proses.js"); ?>"></script>
        <script>
            var post_gambar  = {"form": {"Base64": ""}};
            
            $(document).ready(function(){
                GetDataTable();
            });

            function GetDataTable(){
                var kywd = $("#FrmSearch").find(".kywd").val(), sort = $("#FrmFilter").find(".sort").val();
                request["Page"] = 1;
                request["Sort"] = sort;
                request["Search"] = kywd;
                pagename = "/kelas/ajax_kelas";
                target_table = "kelas";
                GetData(request,"listdatahtml",target_table,function(resp){

                }, 3);
                return false;
            }

            $("#FrmSearch").submit(function() {
                GetDataTable();
                return false;
            });
            $("#FrmFilter .sort").change(function(){
                GetDataTable();
            });

            function SimpanTambahKelas(){
                $("#FrmTambahKelas").submit();
            }
            function SimpanEditKelas(){
                $("#FrmEditKelas").submit();
            }

            var FrmTambahKelas = $("#FrmTambahKelas").validate({
                submitHandler: function(form) {
                    laddasubmit = kelas_baru.find(".ladda-button-submit");
                    InsertData(form, function(resp) {
                        GetDataTable();
                    });
                },
                errorPlacement: function (error, element) {
                    if (element.parent(".input-group").length) { 
                        error.insertAfter(element.parent());      // radio/checkbox?
                    } else if (element.hasClass("select2") || element.hasClass("select")) {
                        error.insertAfter(element.next("span"));  // select2
                    } else {                                      
                        error.insertAfter(element);               // default
                    }
                }
            });
            var FrmEditKelas = $("#FrmEditKelas").validate({
                submitHandler: function(form) {
                    laddasubmit = kelas_edit.find(".ladda-button-submit");
                    UpdateData(form, function(resp) {
                        GetDataTable();
                    });
                },
                errorPlacement: function (error, element) {
                    if (element.parent(".input-group").length) { 
                        error.insertAfter(element.parent());      // radio/checkbox?
                    } else if (element.hasClass("select2") || element.hasClass("select")) {
                        error.insertAfter(element.next("span"));  // select2
                    } else {                                      
                        error.insertAfter(element);               // default
                    }
                }
            });
            var FrmHapusKelas = $("#FrmHapusKelas").validate({
                submitHandler: function(form) {
                    laddasubmit = kelas_hapus.find(".ladda-button-submit");
                    DeleteData(form, function(resp) {
                        GetDataTable();
                    });
                },
                errorPlacement: function (error, element) {
                    if (element.parent(".input-group").length) { 
                        error.insertAfter(element.parent());      // radio/checkbox?
                    } else if (element.hasClass("select2") || element.hasClass("select")) {
                        error.insertAfter(element.next("span"));  // select2
                    } else {                                      
                        error.insertAfter(element);               // default
                    }
                }
            });


            var kelas_detail = $("#modal-detail-kelas");
            var kelas_baru = $("#modal-tambah-kelas");
            var kelas_edit = $("#modal-edit-kelas");
            var kelas_hapus = $("#modal-hapus-kelas");
            $("#modal-tambah-kelas").on('show.bs.modal', function () {
                kelas_baru.find(".data-user").html("<center><img class='loading-gif-image' src='"+base_url+"/assets/img/loading-data.gif' alt='Loading ...'></center>");
                $.ajax({
                    type: "POST",
                    url: base_url + "/data_user/ajax_data_user",
                    data: {act:"getdataall"},
                    dataType: "JSON",
                    tryCount: 0,
                    retryLimit : 3,
                    success: function(resp){
                        if(resp.length == 0 || resp.data == "") {
                            $(".modal").modal("hide");
                            toastrshow("warning", "Tidak ada User", "Peringatan");
                        } else {
                            var html_list_user = "<div class='col-12 row' style='margin-left: 0px; margin-right: 0px;'>";
                            var index_data = 0;
                            $.each(resp.data, function(index, item) {
                                if(item.foto == "" || item.foto == null){
                                    foto = "<?php echo base_url("assets/img/default-user.jpg"); ?>";
                                } else {
                                    foto = "<?php echo base_url("assets/cdn/"); ?>"+item.foto;
                                }
                                if(item.email == "" || item.email == null){
                                    email = "-";
                                } else {
                                    email = item.email;
                                }
                                html_list_user += "<div class='col-6 col-sm-4 col-md-3 nopad text-center'><label class='image-checkbox'><img src='"+foto+"'><input type='checkbox' class='user-checkbox' value='"+item.id+"'><i class='fa fa-check d-none'></i><label class='label-14' style='font-weight: bold;'>"+item.nama+"</label><br><label class='label-14'>"+email+"</label></div>";
                                index_data++;
                            });
                            html_list_user += "</div>";
                            kelas_baru.find(".data-user").html(html_list_user);
                            kelas_baru.find(".image-checkbox").each(function () {
                                if($(this).find('input[type="checkbox"]').first().attr("checked")) {
                                    $(this).addClass('image-checkbox-checked');
                                }
                                else {
                                    $(this).removeClass('image-checkbox-checked');
                                }
                            });
                            kelas_baru.find(".image-checkbox").on("click", function (e) {
                                $(this).toggleClass('image-checkbox-checked');
                                var $checkbox = $(this).find('input[type="checkbox"]');
                                $checkbox.prop("checked",!$checkbox.prop("checked"))
                                e.preventDefault();

                                var checked_data = [];
                                $.each(kelas_baru.find("input[type='checkbox']:checked"), function(){            
                                    checked_data.push($(this).val());
                                });
                                kelas_baru.find(".id-user").val(checked_data.join(", "));
                            });
                        }
                    },
                    error: function(xhr, textstatus, errorthrown) {
                        $(".modal").modal("hide");
                        toastrshow("warning", "Periksa koneksi internet anda kembali", "Peringatan");
                    }
                });
                kelas_baru.find("input[name='form[nama]'], input[name='form[keterangan]'], input[name='form[id_user]']").val("");
                $("#modal-tambah-kelas").on('shown.bs.modal', function () {
                    kelas_baru.find("input[name='form[nama]']").focus();
                });
            });
            $(".datatable-kelas").on("click", ".edit-kelas", function() {
                $("#modal-edit-kelas .modal-title").text("Edit Kelas : Nama Kelas");
                kelas_edit.find(".loading-gif-image").removeClass("d-none");
                kelas_edit.find(".after-loading").addClass("d-none");
                $("#modal-edit-kelas").modal("show");
                var id_update = $(this).attr("data-id");
                kelas_edit.find(".id_hidden").val(id_update);
                pagename = "/kelas/ajax_kelas";
                GetDataById(id_update, function(resp) {
                    var resp = resp.data[0];
                    kelas_edit.find(".data-user").html("<center><img class='loading-gif-image' src='"+base_url+"/assets/img/loading-data.gif' alt='Loading ...'></center>");
                    $.ajax({
                        type: "POST",
                        url: base_url + "/data_user/ajax_data_user",
                        data: {act:"getdataall"},
                        dataType: "JSON",
                        tryCount: 0,
                        retryLimit : 3,
                        success: function(respon_user){
                            if(respon_user.length == 0 || respon_user.data == "") {
                                $(".modal").modal("hide");
                                toastrshow("warning", "Tidak ada User", "Peringatan");
                            } else {
                                var html_list_user = "<div class='col-12 row' style='margin-left: 0px; margin-right: 0px;'>";
                                var index_data = 0;
                                $.each(respon_user.data, function(index, item) {
                                    if(item.foto == "" || item.foto == null){
                                        foto = "<?php echo base_url("assets/img/default-user.jpg"); ?>";
                                    } else {
                                        foto = "<?php echo base_url("assets/cdn/"); ?>"+item.foto;
                                    }
                                    if(item.email == "" || item.email == null){
                                        email = "-";
                                    } else {
                                        email = item.email;
                                    }
                                    var is_checked = "";
                                    if(resp.id_user == "" || resp.id_user == null){
                                        is_checked = "";
                                    } else {
                                        var match = resp.id_user.split(", ");
                                        for (var a in match){
                                            var variable = match[a]
                                            if(variable == item.id){
                                                is_checked = "checked";
                                                break;
                                            } else {
                                                is_checked = "";
                                            }
                                        }
                                    }
                                    html_list_user += "<div class='col-6 col-sm-4 col-md-3 nopad text-center'><label class='image-checkbox'><img src='"+foto+"'><input type='checkbox' class='user-checkbox' value='"+item.id+"' "+is_checked+"><i class='fa fa-check d-none'></i><label class='label-14' style='font-weight: bold;'>"+item.nama+"</label><br><label class='label-14'>"+email+"</label></div>";
                                    index_data++;
                                });
                                html_list_user += "</div>";
                                kelas_edit.find(".data-user").html(html_list_user);
                                kelas_edit.find(".image-checkbox").each(function () {
                                    if($(this).find('input[type="checkbox"]').first().attr("checked")) {
                                        $(this).addClass('image-checkbox-checked');
                                    }
                                    else {
                                        $(this).removeClass('image-checkbox-checked');
                                    }
                                });
                                kelas_edit.find(".image-checkbox").on("click", function (e) {
                                    $(this).toggleClass('image-checkbox-checked');
                                    var $checkbox = $(this).find('input[type="checkbox"]');
                                    $checkbox.prop("checked",!$checkbox.prop("checked"))
                                    e.preventDefault();

                                    var checked_data = [];
                                    $.each(kelas_edit.find("input[type='checkbox']:checked"), function(){            
                                        checked_data.push($(this).val());
                                    });
                                    kelas_edit.find(".id-user").val(checked_data.join(", "));
                                });
                            }
                            kelas_edit.find("input[name='form[nama]']").val(resp.nama);
                            kelas_edit.find("input[name='form[keterangan]']").val(resp.keterangan);
                            kelas_edit.find("input[name='form[id_user]']").val(resp.id_user);
                            kelas_edit.find(".loading-gif-image").addClass("d-none");
                            kelas_edit.find(".after-loading").removeClass("d-none");
                            kelas_edit.find("input[name='form[nama]']").focus();
                            $("#modal-edit-kelas .modal-title").text("Edit Kelas : " + resp.nama);
                        },
                        error: function(xhr, textstatus, errorthrown) {
                            $(".modal").modal("hide");
                            toastrshow("warning", "Periksa koneksi internet anda kembali", "Peringatan");
                        }
                    });
                });
            });
            $(".datatable-kelas").on("click", ".hapus-kelas", function() {
                $("#modal-hapus-kelas .modal-title").text("Hapus Kelas : Nama Kelas");
                kelas_hapus.find(".loading-gif-image").removeClass("d-none");
                kelas_hapus.find(".after-loading").addClass("d-none");
                $("#modal-hapus-kelas").modal("show");
                var id_update = $(this).attr("data-id");
                kelas_hapus.find(".id_hidden").val(id_update);
                pagename = "/kelas/ajax_kelas";
                GetDataById(id_update, function(resp) {
                    var resp = resp.data[0];
                    kelas_hapus.find(".loading-gif-image").addClass("d-none");
                    kelas_hapus.find(".after-loading").removeClass("d-none");
                    $("#modal-hapus-kelas .modal-title").text("Hapus Kelas : " + resp.nama);
                });
            });
            $(".datatable-kelas").on("click", ".detail-kelas", function() {
                $("#modal-detail-kelas .modal-title").text("Detail Kelas : Nama Kelas");
                kelas_detail.find(".loading-gif-image").removeClass("d-none");
                kelas_detail.find(".after-loading").addClass("d-none");
                $("#modal-detail-kelas").modal("show");
                var id_update = $(this).attr("data-id");
                kelas_detail.find(".id_hidden").val(id_update);
                pagename = "/kelas/ajax_kelas";
                GetDataById(id_update, function(resp) {
                    var resp = resp.data[0];
                    $.ajax({
                        type: "POST",
                        url: base_url + "/data_user/ajax_data_user",
                        data: {act:"getdataall"},
                        dataType: "JSON",
                        tryCount: 0,
                        retryLimit : 3,
                        success: function(respon_user){
                            if(respon_user.length == 0 || respon_user.data == "") {
                                $(".modal").modal("hide");
                                toastrshow("warning", "Tidak ada User", "Peringatan");
                            } else {
                                var html_list_user = "<div class='col-12 row' style='margin-left: 0px; margin-right: 0px;'>";
                                var index_data = 0;
                                $.each(respon_user.data, function(index, item) {
                                    if(item.foto == "" || item.foto == null){
                                        foto = "<?php echo base_url("assets/img/default-user.jpg"); ?>";
                                    } else {
                                        foto = "<?php echo base_url("assets/cdn/"); ?>"+item.foto;
                                    }
                                    if(item.email == "" || item.email == null){
                                        email = "-";
                                    } else {
                                        email = item.email;
                                    }
                                    var is_checked = "";
                                    if(resp.id_user == "" || resp.id_user == null){
                                        is_checked = "";
                                    } else {
                                        var match = resp.id_user.split(", ");
                                        for (var a in match){
                                            var variable = match[a]
                                            if(variable == item.id){
                                                html_list_user += "<div class='col-6 col-sm-4 col-md-3 nopad text-center'><label class='image-checkbox'><img src='"+foto+"'><label class='label-14' style='font-weight: bold;'>"+item.nama+"</label><br><label class='label-14'>"+email+"</label></div>";
                                                index_data++;
                                                break;
                                            } else {
                                                is_checked = "";
                                            }
                                        }
                                    }
                                });
                                html_list_user += "</div>";
                                kelas_detail.find(".data-user").html(html_list_user);
                            }
                            kelas_detail.find(".nama_kelas").text(resp.nama);
                            kelas_detail.find(".keterangan").text(resp.keterangan);
                            kelas_detail.find(".loading-gif-image").addClass("d-none");
                            kelas_detail.find(".after-loading").removeClass("d-none");
                            $("#modal-detail-kelas .modal-title").text("Detail Kelas : " + resp.nama);
                        },
                        error: function(xhr, textstatus, errorthrown) {
                            $(".modal").modal("hide");
                            toastrshow("warning", "Periksa koneksi internet anda kembali", "Peringatan");
                        }
                    });
                });
            });
        </script>
    </body>
</html>