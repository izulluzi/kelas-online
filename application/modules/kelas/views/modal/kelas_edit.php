<div class="modal fade" id="modal-edit-kelas" role="dialog" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header bg-primary">
                <h5 class="modal-title text-white">Edit Kelas : Kelas</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true" style="color: white;">&times;</span>
                </button>
            </div>
            <div class="modal-body bg-secondary">
                <form id="FrmEditKelas" class="form-horizontal form-kelas-edit" role="form" method="POST" action="/kelas/ajax_kelas">
                    <center>
                        <img class="loading-gif-image" src="<?php echo base_url("assets/img/loading-data.gif") ?>" alt="Loading ...">
                    </center>
                    <div class="form-group after-loading d-none">
                        <label class="form-control-label">Nama <span class="text-danger">*</span></label>
                        <input required type="text" class="form-control form-control-alternative" placeholder="Nama" name="form[nama]" maxlength="255">
                    </div>
                    <div class="form-group after-loading d-none">
                        <label class="form-control-label">Keterangan</label>
                        <input type="text" class="form-control form-control-alternative" placeholder="Keterangan" name="form[keterangan]" maxlength="255">
                    </div>
                    <div class="form-group after-loading d-none">
                        <label class="form-control-label">User</label>
                        <div class="data-user">
                            <center>
                                <img class="loading-gif-image" src="<?php echo base_url("assets/img/loading-data.gif") ?>" alt="Loading ...">
                            </center>
                        </div>
                    </div>
                    <input type="hidden" class="id-user" name="form[id_user]">
                    <input type="hidden" class="id_hidden" name="form[id]" value="" placeholder="id_data">
                </form>
            </div>
            <div class="modal-footer bg-primary after-loading d-none">
                <button class="btn btn-light" data-dismiss="modal">Batal</button>
                <button type="submit" class="btn btn-neutral ladda-button ladda-button-submit" onclick="SimpanEditKelas();" data-style="slide-up">Simpan</button>
            </div>
        </div>
    </div>
</div>