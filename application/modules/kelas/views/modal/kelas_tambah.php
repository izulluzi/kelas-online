<div class="modal fade" id="modal-tambah-kelas" role="dialog" aria-d-none="true">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header bg-primary">
                <h5 class="modal-title text-white">Tambah Kelas</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-d-none="true" style="color: white;">&times;</span>
                </button>
            </div>
            <div class="modal-body bg-secondary">
                <form id="FrmTambahKelas" class="form-horizontal form-kelas-baru" role="form" method="POST" action="/kelas/ajax_kelas">
                    <div class="form-group">
                        <label class="form-control-label">Nama <span class="text-danger">*</span></label>
                        <input required type="text" class="form-control form-control-alternative" placeholder="Nama" name="form[nama]" maxlength="255">
                    </div>
                    <div class="form-group">
                        <label class="form-control-label">Keterangan</label>
                        <input type="text" class="form-control form-control-alternative" placeholder="Keterangan" name="form[keterangan]" maxlength="255">
                    </div>
                    <div class="form-group">
                        <label class="form-control-label">User</label>
                        <div class="data-user">
                            <center>
                                <img class="loading-gif-image" src="<?php echo base_url("assets/img/loading-data.gif") ?>" alt="Loading ...">
                            </center>
                        </div>
                    </div>
                    <input type="hidden" class="id-user" name="form[id_user]">
                </form>
            </div>
            <div class="modal-footer bg-primary">
                <button class="btn btn-light" data-dismiss="modal">Batal</button>
                <button type="submit" class="btn btn-neutral ladda-button ladda-button-submit" onclick="SimpanTambahKelas();" data-style="slide-up">Simpan</button>
            </div>
        </div>
    </div>
</div>