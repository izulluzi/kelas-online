<div class="modal fade" id="modal-detail-kelas" role="dialog" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header bg-primary">
                <h5 class="modal-title text-white">Detail Kelas : Kelas</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true" style="color: white;">&times;</span>
                </button>
            </div>
            <div class="modal-body bg-secondary">
                <center>
                    <img class="loading-gif-image" src="<?php echo base_url("assets/img/loading-data.gif") ?>" alt="Loading ...">
                </center>
                <div class="after-loading">
                    <ul class="list-group">
                        <li class="list-group-item">    
                            <div class="profile-desc-link">
                                <div class="row">
                                    <div class="col-3 bold font-15">Nama Kelas</div>
                                    <div class="col-9">
                                        <label class="nama_kelas">Nama Kelas</label>
                                    </div>
                                </div>
                            </div> 
                        </li>
                        <li class="list-group-item">    
                            <div class="profile-desc-link">
                                <div class="row">
                                    <div class="col-3 bold font-15">Keterangan</div>
                                    <div class="col-9">
                                        <label class="keterangan">Keterangan</label>
                                    </div>
                                </div>
                            </div> 
                        </li>
                        <div class="data-user">
                            <center>
                                <img class="loading-gif-image" src="<?php echo base_url("assets/img/loading-data.gif") ?>" alt="Loading ...">
                            </center>
                        </div>
                        <br>
                    </ul>
                </div>
            </div>
        </div>
    </div>
</div>