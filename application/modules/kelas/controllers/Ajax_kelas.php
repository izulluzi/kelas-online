<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Ajax_kelas extends MY_Controller
{
    public function __construct() {
        parent::__construct();
        $this->load->model("kelas/kelas");
    }
    public function index() {
        $act = $this->input->post("act");
        $form = $this->input->post("form");
        $this->req  = $this->input->post("req");
        $this->form = $this->input->post("form");
        print_r($this->$act());
    }

    private function listdatahtml() {
        $Request = $this->kelas->HtmlKelas($this->req);
        return $Request;
    }

    private function getdatabyid() {
        $Request = $this->kelas->GetKelas(["filter" => ["id" => $this->req]]);
        return json_encode($Request);
    }

    private function datadropdown() {
        $Request = $this->kelas->DropdownKelas($this->req);
        return $Request;
    }

    private function insertdata() {
        $Request = $this->kelas->InsertKelas($this->form);
        return $Request;
    }

    private function updatedata() {
        $id_update = $this->form["id"]; unset($this->form["id"]);
        $Request = $this->kelas->UpdateKelas($id_update, $this->form);
        return $Request;
    }

    private function deletedata() {
        $id_update = $this->form["id"]; unset($this->form["id"]);
        $Request = $this->kelas->HapusKelas($id_update);
        return $Request;
    }
}
