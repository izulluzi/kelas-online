<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class User extends MY_Controller
{
	function __construct()
	{
		parent::__construct();
	}
	public function login(){
		if(!empty($this->session->userdata("user"))) {
            // redirect("absensi/dashboard");
        } else {
            $this->load->view('user/login.php');
        }
	}

	public function logout() {
        $this->session->sess_destroy();
        redirect("user/login");
    }

    public function edit_profil(){
		$this->foglobal->CheckSessionLogin();
		if($this->session->userdata("user_login")->role == 1){
	        $this->load->view('user/edit_profil_admin.php');	
		}
		if($this->session->userdata("user_login")->role == 2){
	        $this->load->view('user/edit_profil_pelanggan.php');	
		}
	}
}