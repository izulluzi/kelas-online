<?php
    $userlogin = $this->session->userdata("user");
?>
<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <meta name="description" content="Start your development with a Dashboard for Bootstrap 4.">
        <meta name="author" content="Creative Tim">
        <title>Edit Profil</title>
        <link href="<?php echo base_url("assets/img/brand/favicon.png"); ?>" rel="icon" type="image/png">
        <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700" rel="stylesheet">
        <link href="<?php echo base_url("assets/vendor/nucleo/css/nucleo.css"); ?>" rel="stylesheet">
        <link href="<?php echo base_url("assets/vendor/@fortawesome/fontawesome-free/css/all.min.css"); ?>" rel="stylesheet">
        <link href="<?php echo base_url("assets/plugins/select2/select2.min.css"); ?>" rel="stylesheet">
        <link href="<?php echo base_url("assets/plugins/select2/select2-bootstrap.min.css"); ?>" rel="stylesheet">
        <link href="<?php echo base_url("assets/plugins/ladda/ladda-themeless.min.css"); ?>" rel="stylesheet">
        <link href="<?php echo base_url("assets/plugins/toastr/toastr.min.css"); ?>" rel="stylesheet">
        <link href="<?php echo base_url("assets/css/bootstrap-datepicker.min.css"); ?>" rel="stylesheet">
        <link href="<?php echo base_url("assets/css/argon.css?v=1.0.0"); ?>" rel="stylesheet">
        <link href="<?php echo base_url("assets/css/custom.css"); ?>" rel="stylesheet">
    </head>
    <body>
    <?php $this->load->view("other/sidebar"); ?>
    <div class="main-content">
        <?php $this->load->view("other/header"); ?>
        <div class="header bg-primary pb-7">
            <div class="container-fluid">
                <div class="header-body">
                    <div class="row align-items-center head-padding-setup">
                        <div class="col-lg-10 col-md-8">
                            <nav aria-label="breadcrumb"">
                                <ol class="breadcrumb breadcrumb-links breadcrumb-dark">
                                    <li class="breadcrumb-item"><i class="fa fa-user"></i></a></li>
                                    <li class="breadcrumb-item">Edit Profil</li>
                                    <li class="breadcrumb-item nama_pelanggan active" aria-current="page">Nama Profil</li>
                                </ol>
                            </nav>
                        </div>
                        <div class="col-lg-2 col-md-4 text-right">
                            <div class="form-group float-right div-header-button">
                                <button type="button" class="btn btn-sm btn-neutral" onclick="location.reload();"><i class="fa fa-sync-alt" title="Refresh"></i></button>
                                <button type="button" class="btn btn-sm btn-neutral ladda-button ladda-button-submit" data-style="slide-up" onclick="SimpanEditPelanggan();"><i class="fa fa-save"></i></button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="container-fluid mt--7">
            <div class="row">
                <div class="col">
                    <div class="card bg-secondary shadow card-edit-user-pelanggan">
                        <div class="card-body loading-gif-image">
                            <center>
                                <img class="loading-gif-image" src="<?php echo base_url("assets/img/loading-data.gif") ?>" alt="Loading ...">
                            </center>
                        </div>
                        <div class="card-body after-loading d-none">
                            <form id="FrmEditPelanggan" class="form-horizontal form-pelanggan-edit" role="form" method="POST" action="/pelanggan/ajax_pelanggan">
                                <div class="row">
                                    <div class="col-sm-6">
                                        <div class="form-group after-loading d-none">
                                            <label class="form-control-label">Foto <span class="text-danger">*</span></label>
                                            <input type="file" class="form-control form-control-alternative foto" placeholder="Foto">
                                            <br class="strip-foto d-none">
                                            <label class="loading-foto d-none"><i class="fa fa-spin fa-sync-alt"></i> </label><button style="margin:2px;" type="button" class="btn btn-sm btn-primary detail-foto d-none"><i class="fa fa-external-link-alt"></i> Lihat File</button><button style="margin:2px;" type="button" class="btn btn-sm btn-danger hapus-foto d-none"><i class="fa fa-trash"></i> Hapus File</button>
                                            <input class="foto_hidden" type="hidden" name="form[foto]" value="">
                                        </div>
                                        <div class="form-group after-loading d-none">
                                            <label class="form-control-label">Nama <span class="text-danger">*</span></label>
                                            <input required type="text" class="form-control form-control-alternative" placeholder="Nama" name="form[nama]" maxlength="255">
                                        </div>
                                        <div class="form-group after-loading d-none">
                                            <label class="form-control-label">Username <span class="text-danger">*</span></label>
                                            <input required type="text" class="form-control form-control-alternative" placeholder="Username" name="form[username]" maxlength="30">
                                        </div>
                                        <div class="form-group col-lg-9">
                                            <a class="password-pelanggan" data-id=""><i class="fa fa-unlock-alt"></i> Edit Password</a>
                                        </div>
                                    </div>
                                    <div class="col-sm-6">
                                        <div class="form-group after-loading d-none">
                                            <label class="form-control-label">Jenis Kelamin <span class="text-danger">*</span></label>
                                            <select required class="form-control form-control-alternative select" name="form[jk]">
                                                <option value="1">Laki-Laki</option>
                                                <option value="2">Perempuan</option>
                                            </select>
                                        </div>
                                        <div class="form-group after-loading d-none">
                                            <label class="form-control-label">No Telp HP</label>
                                            <input type="text" class="form-control form-control-alternative" placeholder="No Telp HP" name="form[no_telp_hp]" maxlength="20">
                                        </div>
                                        <div class="form-group after-loading d-none">
                                            <label class="form-control-label">Alamat</label>
                                            <input type="alamat" class="form-control form-control-alternative" placeholder="Alamat" name="form[alamat]" maxlength="255">
                                        </div>
                                    </div>
                                </div>
                                <input type="hidden" class="id_hidden" name="form[id]">
                                <input type="hidden" class="id_hidden" name="form[session]">
                            </form>
                        </div>
                        <div class="card-footer bg-primary text-right after-loading d-none">
                            <button class="btn btn-light" onclick="backAway();">Batal</button>
                            <button type="button" class="btn btn-neutral ladda-button ladda-button-submit" data-style="slide-up" onclick="SimpanEditPelanggan();"><i class="fa fa-save"></i> Simpan</button>
                        </div>
                    </div>
                </div>
            </div>
            <?php $this->load->view("other/footer"); ?>
        </div>
        <?php $this->load->view("pelanggan/modal/pelanggan_password"); ?>
        <script src="<?php echo base_url("assets/vendor/jquery/dist/jquery.min.js"); ?>"></script>
        <script src="<?php echo base_url("assets/vendor/bootstrap/dist/js/bootstrap.bundle.min.js"); ?>"></script>
        <script src="<?php echo base_url("assets/js/argon.js?v=1.0.0"); ?>"></script>
        <script src="<?php echo base_url("assets/js/bootstrap-datepicker.js"); ?>"></script>
        <script src="<?php echo base_url("assets/plugins/validate/jquery.validate.min.js"); ?>"></script>
        <script src="<?php echo base_url("assets/plugins/select2/select2.full.js"); ?>"></script>
        <script src="<?php echo base_url("assets/plugins/toastr/toastr.min.js"); ?>"></script>
        <script src="<?php echo base_url("assets/plugins/ladda/spin.min.js"); ?>"></script>
        <script src="<?php echo base_url("assets/plugins/ladda/ladda.min.js"); ?>"></script>
        <script src="<?php echo base_url("assets/plugins/ladda/ladda.jquery.min.js"); ?>"></script>
        <script src="<?php echo base_url("assets/js/moment.min.js"); ?>"></script>
        <script src="<?php echo base_url("assets/js/proses.js"); ?>"></script>
        <script>
            var user_pelanggan_edit = $(".card-edit-user-pelanggan");
            var post_gambar  = {"form": {"Base64": ""}};
            $(document).ready(function(){
                var id_update = "<?php echo $userlogin->id ?>";
                user_pelanggan_edit.find(".id_hidden").val(id_update);
                user_pelanggan_edit.find(".password-pelanggan").attr("data-id", id_update);
                pagename = "/pelanggan/ajax_pelanggan";
                GetDataById(id_update, function(resp) {
                    var resp = resp.data[0];
                    user_pelanggan_edit.find("input[name='form[nama]']").val(resp.nama);
                    $(".nama_pelanggan").html(resp.nama);
                    user_pelanggan_edit.find("input[name='form[username]']").val(resp.username);
                    user_pelanggan_edit.find("input[name='form[no_telp_hp]']").val(resp.no_telp_hp);
                    user_pelanggan_edit.find("input[name='form[alamat]']").val(resp.alamat);
                    user_pelanggan_edit.find("select[name='form[jk]']").val(resp.jk);
                    if(resp.foto == "" || resp.foto == null){
                    } else {
                        user_pelanggan_edit.find(".detail-foto, .strip-foto, .hapus-foto").removeClass("d-none");
                        user_pelanggan_edit.find(".foto, .loading-foto").addClass("d-none");
                    }
                    user_pelanggan_edit.find("input[name='form[foto]']").val(resp.foto);
                    user_pelanggan_edit.find(".loading-gif-image").addClass("d-none");
                    user_pelanggan_edit.find(".after-loading").removeClass("d-none");
                    user_pelanggan_edit.find("input[name='form[nama]']").focus();
                    user_pelanggan_edit.find(".after-loading").removeClass("d-none");
                    user_pelanggan_edit.find(".loading-gif-image").addClass("d-none");
                    user_pelanggan_edit.find("input[name='form[nama]']").focus();
                });
            });

            function SimpanEditPelanggan(){
                $("#FrmEditPelanggan").submit();
            }
            function SimpanPasswordPelanggan(){
                $("#FrmPasswordPelanggan").submit();
            }
            
            var FrmEditPelanggan = $("#FrmEditPelanggan").validate({
                submitHandler: function(form) {
                    laddasubmit = $("html").find(".ladda-button-submit");
                    UpdateData(form, function(resp) {
                        setTimeout(function(){
                            location.reload();
                        }, 1000);
                    });
                },
                errorPlacement: function (error, element) {
                    if (element.parent(".input-group").length) { 
                        error.insertAfter(element.parent());      // radio/checkbox?
                    } else if (element.hasClass("select2") || element.hasClass("select")) {
                        error.insertAfter(element.next("span"));  // select2
                    } else {                                      
                        error.insertAfter(element);               // default
                    }
                }
            });
            var FrmPasswordPelanggan = $("#FrmPasswordPelanggan").validate({
                submitHandler: function(form) {
                    laddasubmit = pelanggan_password.find(".ladda-button-submit");
                    UpdateData(form, function(resp) {
                        setTimeout(function(){
                            location.reload();
                        }, 1000);
                    });
                },
                errorPlacement: function (error, element) {
                    if (element.parent(".input-group").length) { 
                        error.insertAfter(element.parent());      // radio/checkbox?
                    } else if (element.hasClass("select2") || element.hasClass("select")) {
                        error.insertAfter(element.next("span"));  // select2
                    } else {                                      
                        error.insertAfter(element);               // default
                    }
                }
            });

            var pelanggan_password = $("#modal-password-pelanggan");
            $(".datatable-pelanggan, #FrmEditPelanggan").on("click", ".password-pelanggan", function() {
                $("#modal-password-pelanggan .modal-title").text("Edit Pelanggan : Nama Pelanggan");
                pelanggan_password.find(".loading-gif-image").removeClass("d-none");
                pelanggan_password.find(".after-loading").addClass("d-none");
                $("#modal-password-pelanggan").modal("show");
                var id_update = $(this).attr("data-id");
                pelanggan_password.find(".id_hidden").val(id_update);
                pagename = "/pelanggan/ajax_pelanggan";
                GetDataById(id_update, function(resp) {
                    var resp = resp.data[0];
                    pelanggan_password.find("input[name='form[password]']").val("");
                    pelanggan_password.find(".loading-gif-image").addClass("d-none");
                    pelanggan_password.find(".after-loading").removeClass("d-none");
                    pelanggan_password.find("input[name='form[password]']").focus();
                    $("#modal-password-pelanggan .modal-title").text("Edit Pelanggan : " + resp.nama);
                });
            });

            $(".foto").change(function() {
                var selector = $(this);
                if (this.files && this.files[0]) {
                    var tipefile = this.files[0].type.toString();
                    var filename = this.files[0].name.toString();
                    var tipe = ['image/png',  'image/x-png', 'image/jpeg', 'image/pjpeg'];
                    if(tipe.indexOf(tipefile) == -1) {
                        $(this).val("");
                        toastrshow("warning", "Format salah, pilih file dengan format png/jpg/jpeg", "Peringatan");
                        return;
                    }
                    if((this.files[0].size / 1024) < 2048){
                        var FR = new FileReader();
                        FR.addEventListener("load", function(e) {
                            //var base64 = e.target.result;
                            var base64 = e.target.result.replace("data:"+tipefile+";base64,", '');
                            var ext = filename.split(".").pop();
                            post_gambar["form"]["Base64"] = base64;
                            post_gambar["form"]["Ext"] = ext;
                            console.log(post_gambar);
                            $.ajax({
                                type: "POST",
                                url: base_url + "/tool/ajax_tool",
                                data: {act:"upload_file", form:post_gambar},
                                dataType: "JSON",
                                tryCount: 0,
                                retryLimit: 3,
                                beforeSend: function(){
                                    $(".foto_hidden").val("");
                                    $(".foto").addClass("d-none");
                                    $(".loading-foto").removeClass("d-none");
                                },
                                success: function(respon_file){
                                    if(respon_file.IsError != undefined) {
                                        if(respon_file.ErrorMessage[0].error != undefined) {
                                            toastrshow("warning", respon_file.ErrorMessage[0].error, "Peringatan");
                                        } else {
                                            toastrshow("warning", respon_file.ErrorMessage, "Peringatan");
                                        }
                                    } else {
                                        $(".foto_hidden").val(respon_file.Output);
                                        $(".detail-foto, .strip-foto, .hapus-foto").removeClass("d-none");
                                        $(".foto").addClass("d-none");
                                        $(".loading-foto").addClass("d-none");
                                    }
                                },
                                error: function(xhr, textstatus, errorthrown) {
                                    toastrshow("warning", "Periksa koneksi internet anda kembali", "Peringatan");
                                    $(".hapus-foto").click();
                                    $(".loading-foto").addClass("d-none");
                                }
                            });
                        }); 
                        FR.readAsDataURL(this.files[0]);
                    } else {
                        $(this).val("");
                        toastrshow("warning", "Ukuran file maksimum adalah 2 MB", "Warning");
                    }
                }
            });
            $(".hapus-foto").click(function(){
                $(".foto_hidden").val("");
                $(".detail-foto, .strip-foto, .hapus-foto").addClass("d-none");
                $(".foto").val("").trigger("change");
                $(".foto").removeClass("d-none");
            });
            $(".detail-foto").click(function(){
                window.open("<?php echo base_url("assets/cdn/"); ?>"+$(".foto_hidden").val(), '_blank');
            });
        </script>
    </body>
</html>