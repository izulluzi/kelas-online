<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <meta name="description" content="Start your development with a Dashboard for Bootstrap 4.">
        <meta name="author" content="Creative Tim">
        <title>Login</title>
        <link href="<?php echo base_url("assets/img/brand/favicon.png"); ?>" rel="icon" type="image/png">
        <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700" rel="stylesheet">
        <link href="<?php echo base_url("assets/vendor/nucleo/css/nucleo.css"); ?>" rel="stylesheet">
        <link href="<?php echo base_url("assets/vendor/@fortawesome/fontawesome-free/css/all.min.css"); ?>" rel="stylesheet">
        <link href="<?php echo base_url("assets/plugins/select2/select2.min.css"); ?>" rel="stylesheet">
        <link href="<?php echo base_url("assets/plugins/select2/select2-bootstrap.min.css"); ?>" rel="stylesheet">
        <link href="<?php echo base_url("assets/plugins/ladda/ladda-themeless.min.css"); ?>" rel="stylesheet">
        <link href="<?php echo base_url("assets/plugins/toastr/toastr.min.css"); ?>" rel="stylesheet">
        <link href="<?php echo base_url("assets/css/argon.css?v=1.0.0"); ?>" rel="stylesheet">
        <link href="<?php echo base_url("assets/css/custom.css"); ?>" rel="stylesheet">
    </head>
    <body class="bg-default">
        <div class="main-content">
            <nav class="navbar navbar-top navbar-horizontal navbar-expand-md navbar-dark">
                <div class="container px-4">
                    <a class="navbar-brand" href="">
                        <img src="<?php echo base_url("assets/img/brand/white.png"); ?>"/>
                    </a>
                    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbar-collapse-main" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                        <span class="navbar-toggler-icon"></span>
                    </button>
                    <div class="collapse navbar-collapse" id="navbar-collapse-main">
                        <div class="navbar-collapse-header d-md-none">
                            <div class="row">
                                <div class="col-6 collapse-brand">
                                    <a href="../index.html">
                                        <img src="<?php echo base_url("assets/img/brand/blue.png"); ?>">
                                    </a>
                                </div>
                                <div class="col-6 collapse-close">
                                    <button type="button" class="navbar-toggler" data-toggle="collapse" data-target="#navbar-collapse-main" aria-controls="sidenav-main" aria-expanded="false" aria-label="Toggle sidenav">
                                        <span></span>
                                        <span></span>
                                    </button>
                                </div>
                            </div>
                        </div>
                        <!-- <ul class="navbar-nav ml-auto">
                            <li class="nav-item">
                                <a class="nav-link nav-link-icon" href="../index.html">
                                    <i class="ni ni-planet"></i>
                                    <span class="nav-link-inner--text">Dashboard</span>
                                </a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link nav-link-icon" href="../examples/register.html">
                                    <i class="ni ni-circle-08"></i>
                                    <span class="nav-link-inner--text">Register</span>
                                </a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link nav-link-icon" href="../examples/login.html">
                                    <i class="ni ni-key-25"></i>
                                    <span class="nav-link-inner--text">Login</span>
                                </a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link nav-link-icon" href="../examples/profile.html">
                                    <i class="ni ni-single-02"></i>
                                    <span class="nav-link-inner--text">Profile</span>
                                </a>
                            </li>
                        </ul> -->
                    </div>
                </div>
            </nav>
            <div class="header bg-gradient-primary py-7 py-lg-8">
                <div class="container">
                    <div class="header-body text-center mb-7">
                        <div class="row justify-content-center">
                            <div class="col-lg-5 col-md-6">
                                <h1 class="text-white">Selamat Datang!</h1>
                                <p class="text-lead text-light">Silahkan login terlebih dahulu sebelum melanjutkan.</p>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="separator separator-bottom separator-skew zindex-100">
                    <svg x="0" y="0" viewBox="0 0 2560 100" preserveAspectRatio="none" version="1.1" xmlns="http://www.w3.org/2000/svg">
                        <polygon class="fill-default" points="2560 0 2560 100 0 100"></polygon>
                    </svg>
                </div>
            </div>
            <div class="container mt--9 pb-5">
                <div class="row justify-content-center">
                    <div class="col-lg-5 col-md-7">
                        <div class="card bg-secondary shadow border-0">
                            <!-- <div class="card-header bg-transparent pb-5">
                                <div class="text-muted text-center mt-2 mb-3"><small>Sign in with</small></div>
                                <div class="btn-wrapper text-center">
                                    <a href="#" class="btn btn-neutral btn-icon">
                                        <span class="btn-inner--icon"><img src="<?php echo base_url("assets/img/icons/common/github.svg"); ?>"></span>
                                        <span class="btn-inner--text">Github</span>
                                    </a>
                                    <a href="#" class="btn btn-neutral btn-icon">
                                        <span class="btn-inner--icon"><img src="<?php echo base_url("assets/img/icons/common/google.svg"); ?>"></span>
                                        <span class="btn-inner--text">Google</span>
                                    </a>
                                </div>
                            </div> -->
                            <div class="card-body px-lg-5 py-lg-5"><!-- 
                                <div class="text-center text-muted mb-4">
                                    <small>Or sign in with credentials</small>
                                </div> -->
                                <form id="FrmUserLogin" class="form-user-login" role="form" method="POST" action="/user/ajax_user">
                                    <div class="form-group mb-3">
                                        <div class="input-group input-group-alternative">
                                            <div class="input-group-prepend">
                                                <span class="input-group-text"><i class="fa fa-user"></i></span>
                                            </div>
                                            <input type="text" class="form-control" placeholder="Username" required name="form[username]">
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <div class="input-group input-group-alternative">
                                            <div class="input-group-prepend">
                                                <span class="input-group-text"><i class="fa fa-unlock-alt"></i></span>
                                            </div>
                                            <input type="password" class="form-control" placeholder="Password" required name="form[password]">
                                        </div>
                                    </div>
                                    <div class="form-group recaptcha">
                                        <?php 
                                            echo "<center>".$this->recaptcha->getWidget()."</center>";
                                        ?>
                                    </div>
                                    <div class="text-center">
                                        <button class="btn btn-primary btn-lg btn-shadow ladda-button ladda-button-submit"  data-style="slide-up" type="submit"><i class="fa fa-sign-in-alt"></i> Log In</button>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- <footer class="py-5">
            <div class="container">
                <div class="row align-items-center justify-content-xl-between">
                    <div class="col-xl-6">
                        <div class="copyright text-center text-xl-left text-muted">
                            &copy; 2018 <a href="https://www.creative-tim.com" class="font-weight-bold ml-1" target="_blank">Creative Tim</a>
                        </div>
                    </div>
                    <div class="col-xl-6">
                        <ul class="nav nav-footer justify-content-center justify-content-xl-end">
                            <li class="nav-item">
                                <a href="https://www.creative-tim.com" class="nav-link" target="_blank">Creative Tim</a>
                            </li>
                            <li class="nav-item">
                                <a href="https://www.creative-tim.com/presentation" class="nav-link" target="_blank">About Us</a>
                            </li>
                            <li class="nav-item">
                                <a href="http://blog.creative-tim.com" class="nav-link" target="_blank">Blog</a>
                            </li>
                            <li class="nav-item">
                                <a href="https://github.com/creativetimofficial/argon-dashboard/blob/master/LICENSE.md" class="nav-link" target="_blank">MIT License</a>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
        </footer> -->
        <script src="<?php echo base_url("assets/vendor/jquery/dist/jquery.min.js"); ?>"></script>
        <script src="<?php echo base_url("assets/vendor/bootstrap/dist/js/bootstrap.bundle.min.js"); ?>"></script>
        <script src="<?php echo base_url("assets/js/argon.js?v=1.0.0"); ?>"></script>
        <script src="<?php echo base_url("assets/plugins/validate/jquery.validate.min.js"); ?>"></script>
        <script src="<?php echo base_url("assets/plugins/select2/select2.full.js"); ?>"></script>
        <script src="<?php echo base_url("assets/plugins/toastr/toastr.min.js"); ?>"></script>
        <?php echo $this->recaptcha->getScriptTag(); ?>
        <script src="<?php echo base_url("assets/plugins/ladda/spin.min.js"); ?>"></script>
        <script src="<?php echo base_url("assets/plugins/ladda/ladda.min.js"); ?>"></script>
        <script src="<?php echo base_url("assets/plugins/ladda/ladda.jquery.min.js"); ?>"></script>
        <script src="<?php echo base_url("assets/js/proses.js"); ?>"></script>
        <script>
            var login = $(".form-user-login");
            var FrmUserLogin = $("#FrmUserLogin").validate({
                submitHandler: function(form) {
                    Login(form, function(resp) {
                        if(resp.IsError == true) {
                            grecaptcha.reset();
                            $("#FrmUserLogin").find("input[type=text]").filter(":first").focus();
                        }
                    });
                },
                errorPlacement: function (error, element) {
                    if (element.parent(".input-group").length) { 
                        error.insertAfter(element.parent());      // radio/checkbox?
                    } else if (element.hasClass("select2") || element.hasClass("select2")) {     
                        error.insertAfter(element.next("span"));  // select2
                    } else {                                      
                        error.insertAfter(element);               // default
                    }
                }
            });

            function Login(selectorform, successfunc) {
                successfunc = (typeof successfunc !== 'undefined') ?  successfunc : "";
                laddasubmit = login.find(".ladda-button-submit");
                var captcha    = $("#g-recaptcha-response").val();
                var formdata   = $(selectorform).serialize() + "&captcha=" + captcha + "&act=login";
                $.ajax({
                    type: "POST",
                    url: base_url + "/user/ajax_user",
                    data: formdata,
                    dataType: "JSON",
                    tryCount: 0,
                    retryLimit: 3,
                    beforeSend: function() {
                        laddasubmit.ladda("start");
                    },
                    success: function(resp){
                        if(resp.IsError == false) {
                            window.location.href = base_url + "/data_user.html";
                        } else {
                            laddasubmit.ladda("stop");
                            toastrshow("warning", resp.lsdt, "Peringatan");
                            /*setTimeout(function(){
                                window.location.href = base_url + "/user/login.html";
                            }, 3000);*/
                        }
                        if(successfunc != "") {
                            successfunc(resp);
                        }
                    },
                    error: function(xhr, textstatus, errorthrown) {
                        toastrshow("warning", "Login gagal, mohon periksa koneksi internet anda kembali", "Peringatan");
                        laddasubmit.ladda("stop");
                    }
                });
            }
        </script>
    </body>
</html>