<?php
	defined('BASEPATH') OR exit('No direct script access allowed');
	date_default_timezone_set('Asia/Jakarta');

	class Tool extends CI_Model {

		public function __construct() {
	      	parent::__construct();
	      	$this->load->database();
	      	$this->user = $this->session->userdata("user");
	    }

		public function UploadFile($args) {
		   	$data = base64_decode($args["form"]["Base64"]);
		   	$name_file = date("Y.m.d_H.i.s.").uniqid().".".$args["form"]["Ext"];
			$file = "assets/cdn/".$name_file;
			$success = file_put_contents($file, $data);
			if($success){
				$return_data["Output"] = $name_file;
			} else {
				$return_data["IsError"] = true;
				$return_data["ErrorMessage"] = "File gagal disimpan";
			}
			return json_encode($return_data);
        }
	}
