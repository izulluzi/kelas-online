<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Ajax_tool extends MY_Controller
{
    public function __construct() {
        parent::__construct();
        $this->load->model("tool/tool");
    }
    public function index() {
        $act = $this->input->post("act");
        $form = $this->input->post("form");
        $this->req  = $this->input->post("req");
        $this->form = $this->input->post("form");
        print_r($this->$act());
    }

    private function upload_file() {
        $Request = $this->tool->UploadFile($this->form);
        return $Request;
    }
}
