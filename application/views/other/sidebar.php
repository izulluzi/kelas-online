<?php 
    $userlogin = $this->session->userdata("user");
?>
<nav class="navbar navbar-vertical fixed-left navbar-expand-md navbar-light bg-white" id="sidenav-main">
    <div class="container-fluid">
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#sidenav-collapse-main" aria-controls="sidenav-main" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button>
        <a class="navbar-brand pt-0" href="<?php echo base_url("/assestment.html") ?>">
            <img src="<?php echo base_url("assets/img/brand/blue.png"); ?>" class="navbar-brand-img" alt="...">
        </a>
        <ul class="nav align-items-center d-md-none">
            <li class="nav-item dropdown">
                <a class="nav-link" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                    <div class="media align-items-center">
                        <span class="avatar avatar-sm rounded-circle">
                            <img alt="Image placeholder" src="<?php echo base_url("assets/img/theme/user_default.png"); ?>">
                        </span>
                    </div>
                </a>
                <div class="dropdown-menu dropdown-menu-arrow dropdown-menu-right">
                    <div class=" dropdown-header noti-title">
                        <h6 class="text-overflow m-0"><?php echo $userlogin->nama ?></h6>
                    </div>
                    <a href="<?php echo base_url("/user/edit_profil.html") ?>" class="dropdown-item">
                        <i class="fa fa-user"></i>
                        <span>Edit Profil</span>
                    </a>
                    <a href="<?php echo base_url("/user/logout/") ?>" class="dropdown-item">
                        <i class="fa fa-sign-out-alt"></i>
                        <span>Log Out</span>
                    </a>
                </div>
            </li>
        </ul>
        <div class="collapse navbar-collapse" id="sidenav-collapse-main">
            <div class="navbar-collapse-header d-md-none">
                <div class="row">
                    <div class="col-6 collapse-brand">
                        <a href="../index.html">
                            <img src="<?php echo base_url("assets/img/brand/blue.png"); ?>">
                        </a>
                    </div>
                    <div class="col-6 collapse-close">
                        <button type="button" class="navbar-toggler" data-toggle="collapse" data-target="#sidenav-collapse-main" aria-controls="sidenav-main" aria-expanded="false" aria-label="Toggle sidenav">
                            <span></span>
                            <span></span>
                        </button>
                    </div>
                </div>
            </div>
            <h6 class="navbar-heading text-muted">E-Kelas</h6>
            <hr class="my-3">
            <ul class="navbar-nav">
                <li class="nav-item">
                    <a class="nav-link <?php if(!empty($data_user)){ echo $data_user;} ?>" href="<?php echo base_url("/data_user.html") ?>">
                        <i class="fa fa-user text-primary"></i> User
                    </a>
                </li>
            </ul>
            <ul class="navbar-nav">
                <li class="nav-item">
                    <a class="nav-link <?php if(!empty($kelas)){ echo $kelas;} ?>" href="<?php echo base_url("/kelas.html") ?>">
                        <i class="fa fa-layer-group text-primary"></i> Kelas
                    </a>
                </li>
            </ul>
            <ul class="navbar-nav">
                <li class="nav-item">
                    <a class="nav-link <?php if(!empty($admin)){ echo $admin;} ?>" href="<?php echo base_url("/admin.html") ?>">
                        <i class="fa fa-user-cog text-primary"></i> Admin
                    </a>
                </li>
            </ul>
        </div>
    </div>
</nav>