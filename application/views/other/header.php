<?php 
    $userlogin = $this->session->userdata("user");
    if($userlogin->foto == "" || $userlogin->foto == null){
        $foto_profil = base_url("assets/img/theme/user_default.png");
    } else {
        $foto_profil = base_url("assets/cdn/").$userlogin->foto;
    }
?>
<nav class="navbar navbar-top navbar-expand-md navbar-dark" id="navbar-main">
    <div class="container-fluid">
        <a class="h4 mb-0 text-white text-uppercase" href=""><!-- Dashboard --></a>
        <ul class="navbar-nav align-items-center d-none d-md-flex">
            <li class="nav-item dropdown">
                <a class="nav-link pr-0" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                    <div class="media align-items-center">
                        <span class="avatar avatar-sm rounded-circle">
                            <img alt="Image placeholder" src="<?php echo $foto_profil ?>">
                        </span>
                        <div class="media-body ml-2">
                            <span class="mb-0 text-sm font-weight-bold"><?php echo $userlogin->nama ?></span>
                        </div>
                    </div>
                </a>
                <div class="dropdown-menu dropdown-menu-arrow dropdown-menu-right">
                    <a href="<?php echo base_url("/user/edit_profil.html") ?>" class="dropdown-item">
                        <i class="fa fa-user"></i>
                        <span>Edit Profil</span>
                    </a>
                    <a href="<?php echo base_url("/user/logout/") ?>" class="dropdown-item">
                        <i class="fa fa-sign-out-alt"></i>
                        <span>Log Out</span>
                    </a>
                </div>
            </li>
        </ul>
    </div>
</nav>